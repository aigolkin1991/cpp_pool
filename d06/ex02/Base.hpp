//
// Created by Artem Iholkin on 10/10/18.
//

#ifndef D06_BASE_HPP
#define D06_BASE_HPP

class Base
{
public:
    virtual     ~Base(void);
};

#endif //D06_BASE_HPP
