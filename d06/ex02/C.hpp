//
// Created by Artem Iholkin on 10/10/18.
//

#ifndef D06_C_HPP
#define D06_C_HPP

#include "Base.hpp"

class C: public Base
{
public:
    C(void);
    ~C(void);
};


#endif //D06_C_HPP
