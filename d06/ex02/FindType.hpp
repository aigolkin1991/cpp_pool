//
// Created by Artem Iholkin on 10/10/18.
//

#ifndef D06_FINDTYPE_HPP
#define D06_FINDTYPE_HPP

#include <iostream>
#include "Base.hpp"
#include "A.hpp"
#include "B.hpp"
#include "C.hpp"

Base*   generate(void);
void    identify_from_pointer( Base * p );
void    identify_from_reference( Base & p );

#endif //D06_FINDTYPE_HPP
