//
// Created by Artem Iholkin on 10/10/18.
//

#ifndef D06_A_HPP
#define D06_A_HPP

#include "Base.hpp"

class A: public Base
{
public:
    A(void);
    ~A(void);
};

#endif //D06_A_HPP
