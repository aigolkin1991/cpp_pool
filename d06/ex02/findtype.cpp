//
// Created by Artem Iholkin on 10/10/18.
//

#include "FindType.hpp"

Base*   generate(void){
	switch (rand() % 3){
		case 0:
			return static_cast<Base*>(new A());
		case 1:
			return dynamic_cast<Base*>(new B());
		case 2:
			return dynamic_cast<Base*>(new C());
	}
	return new A;
}

void    identify_from_pointer( Base * p ){
	if(dynamic_cast<A*>(p)){
		std::cout << "A" << std::endl;
	} else if(dynamic_cast<B*>(p)){
		std::cout << "B" << std::endl;
	}else if(dynamic_cast<C*>(p)){
		std::cout << "C" << std::endl;
	}
}

void    identify_from_reference( Base & p ){
	try {
		Base    &r = dynamic_cast<A&>(p);
		(void)r;
		std::cout << "A" << std::endl;
	}catch (std::exception& e){

	}

	try {
		Base    &r = dynamic_cast<B&>(p);
		(void)r;
		std::cout << "B" << std::endl;
	}catch (std::exception& e){

	}

	try {
		Base    &r = dynamic_cast<C&>(p);
		(void)r;
		std::cout << "C" << std::endl;
	}catch (std::exception& e){

	}
}

int     main(){
	srand(time(0));

	Base* b = generate();

	identify_from_pointer(b);
	identify_from_reference(*b);

	return 0;
}