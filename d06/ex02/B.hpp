//
// Created by Artem Iholkin on 10/10/18.
//

#ifndef D06_B_HPP
#define D06_B_HPP

#include "Base.hpp"

class B: public Base
{
public:
    B(void);
    ~B(void);
};


#endif //D06_B_HPP
