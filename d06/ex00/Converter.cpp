//
// Created by Artem Iholkin on 10/10/18.
//
#include "Converter.hpp"

Converter::Converter(char *value) :
_defined_type(""),
_literal_representation(value),
a(0), b(0), c(0.0), d(0.0), is_inf(false), precision(1) {
	return ;
}

Converter::~Converter() { return;}

const char*     Converter::ImpossibleException::what(void) const throw() {
	return ("impossible");
}
Converter::ImpossibleException::ImpossibleException(void) throw() { return ; }
Converter::ImpossibleException::ImpossibleException(const ImpossibleException &src) throw() { *this = src; }
Converter::ImpossibleException::~ImpossibleException(void) throw() { return ; }
Converter::ImpossibleException &Converter::ImpossibleException::operator= (const ImpossibleException &rhs) throw() {
	(void)rhs;
	return (*this);
}

const char*     Converter::NotDisplayableException::what(void) const throw() {
	return ("Non displayable");
}
Converter::NotDisplayableException::NotDisplayableException(void) throw() { return ; }
Converter::NotDisplayableException::NotDisplayableException(const NotDisplayableException &src) throw() { *this = src; }
Converter::NotDisplayableException::~NotDisplayableException(void) throw() { return ; }
Converter::NotDisplayableException &Converter::NotDisplayableException::operator= (const NotDisplayableException &rhs) throw() {
	(void)rhs;
	return (*this);
}

const char*     Converter::NotConvertableException::what(void) const throw(){
	return ("Can't be converted to any type");
}
Converter::NotConvertableException::NotConvertableException(void) throw() { return ; }
Converter::NotConvertableException::NotConvertableException(const NotConvertableException &src) throw() { *this = src; }
Converter::NotConvertableException::~NotConvertableException(void) throw() { return ; }
Converter::NotConvertableException &Converter::NotConvertableException::operator= (const NotConvertableException &rhs) throw() {
	(void)rhs;
	return (*this);
}

std::string             Converter::getDefinedType(void){
	return this->_defined_type;
}

char                    Converter::getChar(void){
	return this->a;
}

int                     Converter::getInt(void){
	return this->b;
}

float                   Converter::getFloat(void){
	return this->c;
}

double                  Converter::getDouble(void){
	return this->d;
}

bool                    Converter::getIsInf(void) {
	return this->is_inf;
}

std::string             Converter::getInfVal(void) {
	return this->inf_val;
}

void                    Converter::setPrecision(std::string literal){
	if(literal[literal.size() - 1] == 'f'){
		this->precision = (((literal.size() - 2 - literal.find_first_of('.')) > 0) ? literal.size() - 2 - literal.find_first_of('.') : 1);
	}else{
		this->precision = (((literal.size() - 1 - literal.find_first_of('.')) > 0) ? literal.size() - 1 - literal.find_first_of('.') : 1);
	}
}

unsigned                Converter::getPrecision(void){
	return this->precision;
}

void            Converter::defineType(void) {
	std::string     literalRepresentation(this->_literal_representation);
	if(this->tryChar()){
		if(strlen(this->_literal_representation) == 4){
			char val = 0;
			if(this->_literal_representation[2] == 'a')
				val = '\a';
			else if(this->_literal_representation[2] == 'b')
				val = '\b';
			else if(this->_literal_representation[2] == 't')
				val = '\b';
			else if(this->_literal_representation[2] == 'n')
				val = '\b';
			else if(this->_literal_representation[2] == 'v')
				val = '\b';
			else if(this->_literal_representation[2] == 'f')
				val = '\b';
			else if(this->_literal_representation[2] == 'r')
				val = '\b';
			else
				val = '\0';
			this->a = val;
			this->_defined_type = "char";
		}else{
			char val = this->_literal_representation[1];
			this->a = val;
			this->_defined_type = "char";
		}
	}else if(this->tryInt()){
		int     val = std::stoi(literalRepresentation);
		this->b = val;
		this->_defined_type = "int";
	}else if(this->tryFloat()){
		if(literalRepresentation == "-inff" || literalRepresentation == "+inff" || literalRepresentation == "nanf"){
			this->c = 0.0;
			this->_defined_type = "float";
			this->is_inf = true;
			this->inf_val = literalRepresentation;
		}else{
			float   val = std::stof(literalRepresentation);
			this->c = val;
			this->_defined_type = "float";
			this->setPrecision(literalRepresentation);
		}
	}else if(this->tryDouble()){
		if(literalRepresentation == "-inf" || literalRepresentation == "+inf" || literalRepresentation == "nan"){
			this->d = 0.0;
			this->_defined_type = "double";
			this->is_inf = true;
			this->inf_val = literalRepresentation;
		}else{
			double  val = std::stod(literalRepresentation);
			this->d = val;
			this->_defined_type = "double";
			this->setPrecision(literalRepresentation);
		}
	}else{
		throw NotConvertableException();
	}


}



bool            Converter::tryChar(void){
	bool            is_char = false;
	unsigned        size = strlen(this->_literal_representation);
	std::string     escape_characters("abtnvfr0");

	if(size == 3){
		if(this->_literal_representation[0] == '\'' && this->_literal_representation[2] == '\''){
			return true;
		}
	}else if(size == 4){
		if(this->_literal_representation[0] == '\'' && this->_literal_representation[3] == '\''){
			if(this->_literal_representation[1] == '\\'){
				for(unsigned i = 0; i < escape_characters.size(); i++){
					if(this->_literal_representation[2] == escape_characters[i]){
						return true;
					}
				}
			}
		}
	}
	return is_char;
}
bool            Converter::tryInt(void){
	bool            is_int = true;
	unsigned        size = strlen(this->_literal_representation);
	std::string     integers("-+0123456789");

	for (unsigned i = 0; i < size; i++){
		if(this->_literal_representation[i] == '+' || this->_literal_representation[i] == '-'){
			if(i != 0){
				return false;
			}
		}
		if(integers.find(this->_literal_representation[i]) == std::string::npos){
			return false;
		}
	}
	std::stringstream ss;
	ss << atoi(this->_literal_representation);
	if(std::string(this->_literal_representation) != (ss.str())
	&& (std::string(this->_literal_representation) != ("+"+(ss.str())))){
		return false;
	}
	return is_int;
}
bool            Converter::tryFloat(void){
	bool                    is_float = false;
	std::string             literalRepresentation(this->_literal_representation);
	if(literalRepresentation == "-inff" || literalRepresentation == "+inff" || literalRepresentation == "nanf"){
		return true;
	}

	if(literalRepresentation.find_first_of('.') == literalRepresentation.find_last_of('.')){
		if(literalRepresentation.find_first_of('.') > 0){
			if(literalRepresentation[literalRepresentation.size() - 1] == 'f'
			&& (literalRepresentation.find_first_of('f') == literalRepresentation.find_last_of('f'))){
				if((literalRepresentation.find_last_of('-') != 0 && literalRepresentation.find_last_of('-') != std::string::npos)
				|| ((literalRepresentation.find_last_of('+') != 0) && (literalRepresentation.find_last_of('+') != std::string::npos))){
					return false;
				}else{
					std::string   float_parts("0123456789-+f.");
					for (unsigned i = 0; i < literalRepresentation.size(); i ++){
						if(float_parts.find_first_of(literalRepresentation[i]) == std::string::npos){
							return false;
						}
					}
					return true;
				}
			}
		}
	}
	return is_float;
}
bool            Converter::tryDouble(void){
	bool    is_double = false;

	std::string     literalRepresentation = this->_literal_representation;
	if(literalRepresentation == "-inf" || literalRepresentation == "+inf" || literalRepresentation == "nan"){
		return true;
	}

	if(literalRepresentation.find_first_of('.') == literalRepresentation.find_last_of('.')){
		if(literalRepresentation.find_first_of('.') > 0){
			if(literalRepresentation[literalRepresentation.size() - 1] != 'f'
			   && (literalRepresentation.find_first_of('f') == literalRepresentation.find_last_of('f'))){
				if((literalRepresentation.find_last_of('-') != 0 && literalRepresentation.find_last_of('-') != std::string::npos)
				   || ((literalRepresentation.find_last_of('+') != 0) && (literalRepresentation.find_last_of('+') != std::string::npos))){
					return false;
				}else{
					std::string   float_parts("0123456789-+.");
					for (unsigned i = 0; i < literalRepresentation.size(); i ++){
						if(float_parts.find_first_of(literalRepresentation[i]) == std::string::npos){
							return false;
						}
					}
					return true;
				}
			}
		}
	}
	return is_double;
}
