//
// Created by Artem Iholkin on 10/9/18.
//
#include <iostream>
#include <string>
#include <iomanip>
#include "Converter.hpp"

int     main(int ac, char** av){
    if(ac >= 2){
        Converter   converter(av[1]);

        try {
            converter.defineType();
        }catch (std::exception &e){
            std::cout << e.what() << std::endl;
            return 0;
        }

        char a = 0;
        int b = 0;
        float c = 0.0f;
        double d = 0.0;

        std::string definedType(converter.getDefinedType());

        if(definedType == "char"){
            a = converter.getChar();
            c = (static_cast<float>(a));
            d = (static_cast<double>(a));
            std::cout   << "char: " << (isprint(a) ? (std::string()+a) : "Non printable") << std::endl
                        << "int: " << static_cast<int>(a) << std::endl
                        << "float: " << std::fixed << std::showpoint << std::setprecision(1) << c << "f" << std::endl
                        << "double: " << std::fixed << std::showpoint << std::setprecision(1) << d << std::endl;
        }else if(definedType == "int"){
            b = converter.getInt();
            c = static_cast<float>(b);
            d = static_cast<double>(b);
            a = static_cast<char>(b);
            std::cout   << "char: " << (isascii(b) ? (isprint(a) ? (std::string()+a) : "Non printable") : "Impossible") << std::endl
                        << "int: " << b << std::endl
                        << "float: " << std::fixed << std::showpoint << std::setprecision(1) << c << "f" << std::endl
                        << "double: " << std::fixed << std::showpoint << std::setprecision(1) << d << std::endl;
        }else if(definedType == "float"){
            c = converter.getFloat();
            a = static_cast<char>(c);
            b = static_cast<int>(c);
            d = static_cast<double>(c);
            std::cout   << "char: " << (converter.getIsInf() ? "Impossible" : (isascii(b) ? (isprint(a) ? (std::string()+a) : "Non printable") : "Impossible") ) << std::endl
                        << "int: " << (converter.getIsInf() ? "Impossible" : ((c > 2147483647 || c < -2147483648) ? "Impossible" : std::to_string(b))) << std::endl;
            if(converter.getIsInf()){
                std::string infd = converter.getInfVal();
                infd.pop_back();
                std::cout   << "float: " << converter.getInfVal() << std::endl
                            << "double: " << infd << std::endl;
            }else{
                std::cout   << "float: " << std::fixed << std::showpoint << std::setprecision(converter.getPrecision()) << c << "f" << std::endl
                            << "double: " << std::fixed << std::showpoint << std::setprecision(converter.getPrecision()) << d << std::endl;
            }
        }else{
            d = converter.getDouble();
            a = static_cast<char>(d);
            b = static_cast<int>(d);
            c = static_cast<double>(d);
            std::cout   << "char: " << (converter.getIsInf() ? "Impossible" : (isascii(b) ? (isprint(a) ? (std::string()+a) : "Non printable") : "Impossible") ) << std::endl
                        << "int: " << (converter.getIsInf() ? "Impossible" : ((d > 2147483647 || d < -2147483648) ? "Impossible" : std::to_string(b))) << std::endl;
            if(converter.getIsInf()){
                std::cout   << "float: " << converter.getInfVal() << "f" << std::endl
                            << "double: " << converter.getInfVal() << std::endl;
            }else{
                std::cout   << "float: " << std::fixed << std::showpoint << std::setprecision(converter.getPrecision()) << c << "f" << std::endl
                            << "double: " << std::fixed << std::showpoint << std::setprecision(converter.getPrecision()) << d << std::endl;
            }
        }

    }
    return 0;
}