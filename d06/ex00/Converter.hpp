//
// Created by Artem Iholkin on 10/10/18.
//

#ifndef D06_CONVERTER_HPP
#define D06_CONVERTER_HPP

#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
#include <exception>

class Converter{

class ImpossibleException : public std::exception{
public:
        virtual const char* what() const throw();
        ImpossibleException () throw();
        ImpossibleException (const ImpossibleException&) throw();
        ImpossibleException& operator= (const ImpossibleException&) throw();
        virtual ~ImpossibleException() throw();
};

class NotDisplayableException : public std::exception{
public:
	virtual const char* what() const throw();
        NotDisplayableException () throw();
        NotDisplayableException (const NotDisplayableException&) throw();
        NotDisplayableException& operator= (const NotDisplayableException&) throw();
        virtual ~NotDisplayableException() throw();
};

class NotConvertableException : public std::exception{
public:
        virtual const char* what() const throw();
        NotConvertableException () throw();
        NotConvertableException (const NotConvertableException&) throw();
        NotConvertableException& operator= (const NotConvertableException&) throw();
        virtual ~NotConvertableException() throw();
};

public:
	Converter(char* value);
	~Converter(void);

	void                    defineType(void);

	std::string             getDefinedType(void);
        char                    getChar(void);
        int                     getInt(void);
        float                   getFloat(void);
        double                  getDouble(void);
        bool                    getIsInf(void);
        std::string             getInfVal(void);
        unsigned                getPrecision(void);

private:
        std::string             _defined_type;
        char*                   _literal_representation;
        char                    a;
        int                     b;
        float                   c;
        double                  d;
        bool                    is_inf;
        std::string             inf_val;
        unsigned                precision;

        bool                    tryChar(void);
        bool                    tryInt(void);
        bool                    tryFloat(void);
        bool                    tryDouble(void);
        void                    setPrecision(std::string literal);

        Converter(void);
        Converter(Converter const &converter);

        Converter               operator=(Converter const &src);
};

#endif //D06_CONVERTER_HPP
