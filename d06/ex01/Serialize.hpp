//
// Created by Artem Iholkin on 10/10/18.
//

#ifndef D06_SERIALIZE_HPP
#define D06_SERIALIZE_HPP

#include <iostream>
#include <string>

struct  Data{
    std::string s1;
    std::string s2;
    int         n;
};

void    *serialize( void );
Data    *deserialize( void *raw );

#endif //D06_SERIALIZE_HPP
