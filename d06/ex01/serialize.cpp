//
// Created by Artem Iholkin on 10/10/18.
//
#include "Serialize.hpp"

int     main(void){
	srand(time(0));
	void*   data = serialize();
	Data*   data_s = deserialize(data);

	if(data_s){
		std::cout << data_s->s1 << std::endl << data_s->s2 << std::endl << data_s->n << std::endl;
	}

	return 0;
}

void*   serialize( void ){

	std::string     chars("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxz1234567890");
	size_t          size = chars.size();
	char*           serialized = new char [20];

	int             i = 0;
	int             j = 12;

//	std::string     test_1("");
//	std::string     test_2("");

	while (i < 8){
		char c = chars[rand() % (size - 2)];
		serialized[i] = c;
//		test_1 = test_1 + c;
		i++;
	}

	while (j < 20){
		char c = chars[rand() % (size - 2)];
		serialized[j] = c;
//		test_2 = test_2 + c;
		j++;
	}

	int             n = rand();
	char            *nb = reinterpret_cast<char*>(&n);

//	std::cout << "Number is: " << n << std::endl;
//	std::cout << "String 1 is: " << test_1 << std::endl;
//	std::cout << "String 2 is: " << test_2 << std::endl;

	serialized[8] = nb[0];
	serialized[9] = nb[1];
	serialized[10] = nb[2];
	serialized[11] = nb[3];

	return reinterpret_cast<void*>(serialized);
}

Data    *deserialize( void *raw ){
	char*   data_tos = reinterpret_cast<char*>(raw);
	Data*   data = new Data;

	std::string     s1(data_tos, 0, 8);
	std::string     s2("");
	int j = 12;
	while (j < 20){
		s2 = s2 + data_tos[j];
		j++;
	}

	char    pnb[4] = {data_tos[8], data_tos[9], data_tos[10], data_tos[11]};

	int     nb = reinterpret_cast<int&>(pnb);

	data->s1 = s1;
	data->s2 = s2;
	data->n = nb;

	return data;
}