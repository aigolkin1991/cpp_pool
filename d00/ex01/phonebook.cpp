#include <iostream>
#include <string>
#include "Contact.class.hpp"
#include "Phonebook.class.hpp"

int main(){
  	const std::string o_exit_command ("EXIT");
  	const std::string o_add_command ("ADD");
  	const std::string o_search_command("SEARCH");
	Phonebook phonebook_instance; 

	while(true){
		std::string o_command_from_input;
		std::cout << "Enter one of the folowing command\n[EXIT, ADD, SEARCH]" << std::endl;
		getline(std::cin, o_command_from_input);

		if(o_command_from_input.compare(o_exit_command) == 0){
			return (0);
		}else if(o_command_from_input.compare(o_add_command) == 0){
			phonebook_instance.add();
		}else if(o_command_from_input.compare(o_search_command) == 0){
			phonebook_instance.search();
		}
	}
}
