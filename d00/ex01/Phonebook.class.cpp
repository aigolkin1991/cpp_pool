#include <iostream>
#include <iomanip>
#include <cstring>
#include <string>
#include <stdlib.h> 
#include "Contact.class.hpp"
#include "Phonebook.class.hpp"


Phonebook::Phonebook( void ){
	return ;
}

Phonebook::~Phonebook( void ){
	return ;
}

void Phonebook::add( void ){
	for(int i = 0; i < 8; i++){
		if(this->_list_of_contact[i].empty == 1){
			this->_list_of_contact[i].set_data();
			return ;
		}
	}
	std::cout << "No place to add new contact" << std::endl;
	return ;
}

void Phonebook::search( void ){
	int contacts_num = 0;
	std::cout << std::setw(10) << "Index" << "|";
	std::cout << std::setw(10) << "Firstname" << "|";
	std::cout << std::setw(10) << "Lastname" << "|";
	std::cout << std::setw(10) << "Nickname" << std::endl;
	std::cout << std::string(43, '-') << std::endl;
	for(int i = 0; i < 8; i++){
		if(this->_list_of_contact[i].empty != 1){
			this->_list_of_contact[i].print_line_data( i );
			contacts_num++;
		}
	}
	if(contacts_num > 0){
		std::string i("");
		std::cout << "Enter index of contact to get detailed info" << std::endl;
		while(1){
			getline(std::cin, i);
			if(i.size() > 1 || !(i.c_str()[0] >= 48 && i.c_str()[0] <= 55)){
				std::cout << "Invalid index - try again" << std::endl;
			}else{
				int index = atoi(i.c_str());
				if(this->_list_of_contact[index].empty == 1){
					std::cout << "Non existing contact" << std::endl;
				}else{
					this->_list_of_contact[index].print_data();
					break ;
				}
			}
		}
	}else{
		std::cout << "There is no contacts" << std::endl;
	}

	return ;
}
