#include <iostream>
#include <iomanip>
#include <string>
#include "Contact.class.hpp"

Contact::Contact( void ){
	this->_first_name = "";
	this->_last_name = "";
	this->_name = "";
	this->_nickname = "";
	this->_login = "";
	this->_postal_addr = "";
	this->_email = "";
	this->_phone = "";
	this->_birthday = "";
	this->_favorite_meal = "";
	this->_underwear_color = "";
	this->_secret = "";
	this->empty = 1;
	return ;
}

Contact::~Contact( void ){
	return ;
}

void Contact::set_data( void ){
	std::cout << "Input first name" << std::endl;
	this->_first_name = this->_get_input();
	std::cout << "Input last name" << std::endl;
	this->_last_name = this->_get_input();
	std::cout << "Input name" << std::endl;
	this->_name = this->_get_input();
	std::cout << "Input nickname" << std::endl;
	this->_nickname = this->_get_input();
	std::cout << "Input login" << std::endl;
	this->_login = this->_get_input();
	std::cout << "Input postal addres" << std::endl;
	this->_postal_addr = this->_get_input();
	std::cout << "Input email" << std::endl;
	this->_email = this->_get_input();
	std::cout << "Input phone" << std::endl;
	this->_phone = this->_get_input();
	std::cout << "Input birthday date" << std::endl;
	this->_birthday = this->_get_input();
	std::cout << "Write your favorite meal" << std::endl;
	this->_favorite_meal = this->_get_input();
	std::cout << "Write color of your underwear" << std::endl;
	this->_underwear_color = this->_get_input();
	std::cout << "Write your darkest secret" << std::endl;
	this->_secret = this->_get_input(); 

	this->empty = 0;
	return ;
}

void Contact::print_data( void ){
	std::cout << "First name: " << this->_first_name << std::endl;
	std::cout << "Last name: " << this->_last_name << std::endl;
	std::cout << "Name: " << this->_name << std::endl;
	std::cout << "Nickname: " << this->_nickname << std::endl;
	std::cout << "Login: " << this->_login << std::endl;
	std::cout << "Postal addres: " << this->_postal_addr << std::endl;
	std::cout << "Email: " << this->_email << std::endl;
	std::cout << "Phone: " << this->_phone <<std::endl;
	std::cout << "Birth date: " << this->_birthday << std::endl;
	std::cout << "Favorite meal: " << this->_favorite_meal << std::endl;
	std::cout << "Underwear color: " << this->_underwear_color << std::endl;
	std::cout << "darkest secret: " << this->_secret << std::endl;

	return ;
}

void Contact::print_line_data( int index ){
	const std::string replace(".");

	std::cout << std::setw(10) << index << "|";
	std::cout << std::setw(10) << ((this->_first_name.size() > 10) ? this->_first_name.replace(9, this->_first_name.size(), replace) : this->_first_name) << "|";
	std::cout << std::setw(10) << ((this->_last_name.size() > 10) ? this->_last_name.replace(9, this->_last_name.size(), replace) : this->_last_name) << "|";
	std::cout << std::setw(10) << ((this->_nickname.size() > 10) ? this->_nickname.replace(9, this->_nickname.size(), replace) : this->_nickname) << std::endl;
	return ;
}

std::string Contact::_get_input( void ){
	std::string	input;
	while(1){
		getline(std::cin, input);
		if(input.size() > 0){
			return input;
		}else{
			std::cout << "Input can't be blanc" << std::endl;
		}
	}
}
