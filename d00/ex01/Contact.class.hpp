#ifndef CONTACT_HPP
# define CONTACT_HPP

class Contact{
	private:
		std::string	_first_name, _last_name, _name, _nickname, _login, _postal_addr, _email, _phone, _birthday, _favorite_meal, _underwear_color, _secret;

		std::string _get_input( void );

	public:
		int 	empty;

		Contact( void );
		~Contact( void );
		void set_data( void );
		void print_data( void );
		void print_line_data( int index );
};

#endif
