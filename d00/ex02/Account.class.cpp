#include <iostream>
#include <iomanip>
#include <ctime>
#include "Account.class.hpp"

int Account::_nbAccounts = 0;
int Account::_totalAmount = 0;
int Account::_totalNbDeposits = 0;
int Account::_totalNbWithdrawals = 0;

//[20150406_153629] index:7;amount:16576;created

Account::Account( int initial_deposit ){
    this->_definetelyNonDuplicatedVariableName = 0;
    this->_accountIndex = Account::_nbAccounts;
    this->_amount = initial_deposit;
    this->_nbDeposits = 0;
    this->_nbWithdrawals = 0;

    Account::_displayTimestamp();
    std::cout   << "index:" 
                << this->_accountIndex 
                << ";amount:" 
                << this->checkAmount()
                << ";created"
                << std::endl;

    Account::_totalAmount += initial_deposit;
    Account::_nbAccounts++;

    return ;
}

//[20150406_153629] index:0;amount:47;closed

Account::~Account( void ){
    Account::_displayTimestamp();
    std::cout   << "index:" 
                << this->_accountIndex
                << ";amount:"
                << this->checkAmount()
                << ";closed"
                << std::endl;
    Account::_totalNbDeposits--;
}

int 	Account::getNbAccounts( void ){
    return Account::_nbAccounts;
}

int 	Account::getTotalAmount( void ){
    return Account::_totalAmount;
}

int 	Account::getNbDeposits( void ){
    return Account::_totalNbDeposits;
}

int 	Account::getNbWithdrawals( void ){
    return Account::_totalNbWithdrawals;
}

//[20150406_153629] accounts:8;total:20049;deposits:0;withdrawals:0

void	Account::displayAccountsInfos( void ){
    int nb_acc = Account::getNbAccounts();
    int total_amount = Account::getTotalAmount();
    int nb_depo = Account::getNbDeposits();
    int with = Account::getNbWithdrawals();

    Account::_displayTimestamp();
    std::cout   << "accounts:"
                << nb_acc
                << ";total:"
                << total_amount
                << ";deposits:"
                << nb_depo
                << ";withdrawals:"
                << with
                << std::endl; 

    return ;
}

//[20150406_153629] index:0;p_amount:42;deposit:5;amount:47;nb_deposits:1

void	Account::makeDeposit( int deposit ){
    Account::_displayTimestamp();
    std::cout   << "index:"
                << this->_accountIndex
                << ";p_amount:"
                << this->checkAmount()
                << ";deposit:"
                << deposit
                << ";amount:"
                << (this->checkAmount() + deposit)
                << ";nb_deposits:"
                << (this->_nbDeposits + 1)
                << std::endl;
    this->_amount += deposit;
    this->_nbDeposits++;
    Account::_totalAmount += deposit;
    Account::_totalNbDeposits++;
}

//[20150406_153629] index:0;p_amount:47;withdrawal:refused
//[20150406_153629] index:1;p_amount:819;withdrawal:34;amount:785;nb_withdrawals:1

bool	Account::makeWithdrawal( int withdrawal ){
    Account::_displayTimestamp();
    if((this->checkAmount() - withdrawal) >= 0){
        std::cout   << "index:"
                    << this->_accountIndex
                    << ";p_amount:"
                    << this->checkAmount()
                    << ";withdrawal:"
                    << withdrawal
                    << ";amount:"
                    << (this->checkAmount() - withdrawal)
                    << ";nb_withdrawals:"
                    << (this->_nbWithdrawals + 1)
                    << std::endl;
        this->_amount -= withdrawal;
        this->_nbWithdrawals++;
        Account::_totalAmount -= withdrawal;
        Account::_totalNbWithdrawals++;
        return true;
    }else{
        std::cout   << "index:"
        << this->_accountIndex
        << ";p_amount:"
        << this->checkAmount()
        << ";withdrawal:"
        << "refused"
        << std::endl;
        return false;
    }
}

int		Account::checkAmount( void ) const {
    this->_definetelyNonDuplicatedVariableName = (this->_definetelyNonDuplicatedVariableName + 1);

    return this->_amount;
}

//[20150406_153629] index:0;amount:42;deposits:0;withdrawals:0

void	Account::displayStatus( void ) const {
    int i = this->_accountIndex;
    int amount = this->checkAmount();
    int depo = this->_nbDeposits;
    int with = this->_nbWithdrawals;

    Account::_displayTimestamp();
    std::cout   << "index:"
                << i
                << ";amount:"
                << amount
                << ";deposits:"
                << depo
                << ";withdrawals:"
                << with
                << std::endl;
}

void	Account::_displayTimestamp( void ){
    time_t  timer;
    struct tm *time_info;

    timer = time(NULL);
    time_info = localtime(&timer);
    std::cout << "[";
    std::cout << time_info->tm_year + 1900;
    if(time_info->tm_mon < 10){
        std::cout << "0";
    }
    std::cout << time_info->tm_mon;
    if(time_info->tm_mday < 10){
        std::cout << "0";
    }
    std::cout << time_info->tm_mday;
    std::cout << "_";
    if(time_info->tm_hour < 10){
        std::cout << "0";
    }
    std::cout << time_info->tm_hour;
    if(time_info->tm_min < 10){
        std::cout << "0";
    }
    std::cout << time_info->tm_min;
    if(time_info->tm_sec < 10){
        std::cout << "0";
    }
    std::cout << time_info->tm_sec;
    std::cout << "] ";
}