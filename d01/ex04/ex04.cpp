//
// Created by Artem Iholkin on 10/2/18.
//

#include <iostream>
#include <string>

void printByRef(std::string& src){
    std::cout << src << std::endl;
}

void printByPtr(std::string* src){
    std::cout << *src << std::endl;
}

int main( void ){
    std::string str("HI THIS IS BRAIN");
    printByRef(str);
    printByPtr(&str);
    return 0;
}