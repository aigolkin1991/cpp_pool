//
// Created by Artem Iholkin on 10/2/18.
//

#ifndef HUMANA_HPP
# define HUMANA_HPP

class HumanA{

public:
    HumanA( std::string name, Weapon& weapon );
    ~HumanA( void );
    void        attack( void );

private:
    std::string _name;
    Weapon&     _weapon;
};

#endif
