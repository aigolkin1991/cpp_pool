//
// Created by Artem Iholkin on 10/2/18.
//
#include <iostream>
#include <string>
#include "Weapon.hpp"
#include "HumanA.hpp"

HumanA::HumanA(std::string name, Weapon& weapon): _name(name), _weapon(weapon) {
    return ;
}

HumanA::~HumanA( void ) {
    return ;
}

void    HumanA::attack( void ) {
    std::cout << this->_name << " attacs with his " << this->_weapon.getType() << std::endl;
}