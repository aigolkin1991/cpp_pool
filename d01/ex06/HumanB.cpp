//
// Created by Artem Iholkin on 10/2/18.
//
#include <iostream>
#include <string>
#include "Weapon.hpp"
#include "HumanB.hpp"

HumanB::HumanB(std::string name): _name(name) {
    return ;
}

HumanB::~HumanB( void ) {
    return ;
}

void        HumanB::attack( void ) {
    std::cout << this->_name << " attacs with his " << this->_weapon->getType() << std::endl;
    return;
}


void        HumanB::setWeapon( Weapon& weapon ){
    this->_weapon = &weapon;
}