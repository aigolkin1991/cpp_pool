//
// Created by Artem Iholkin on 10/2/18.
//
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

void    printUsage( void ){
    std::cout << "Usage: " << "./replace [file_name] [search_string] [replace_string]" << std::endl;
    return ;
}

void    findAndReplace(std::string* buff, std::string search, std::string replace){
    size_t      ret;
    size_t      replace_size = search.size();

    while ((ret = (buff->find(search, 0))) != (size_t)(-1)){
        buff->replace(ret, replace_size, replace);
    }
}

int     main( int ac, char** av ){
    if(ac != 4){
        std::cout << "Invalid number of arguments - 3 expected, " << (ac - 1) << " provided" << std::endl;
        printUsage();
        return 0;
    }

    std::string s1(av[2]);
    std::string s2(av[3]);
    if(s1.size() == 0 || s2.size() == 0){
        std::cout << "Strings can't be blanc" << std::endl;
        printUsage();
        return 0;
    }

    std::ifstream   ifs(av[1], std::ifstream::binary);
    std::string     buff;

    if(ifs.is_open()){
        std::filebuf*   fbp = ifs.rdbuf();
        std::size_t     size = fbp->pubseekoff (0,ifs.end,ifs.in);
        fbp->pubseekpos (0,ifs.in);
        char*           buffer = new char[size];

        fbp->sgetn(buffer, size);
        buff = buffer;
        ifs.close();

        delete[] buffer;
    }else{
        std::cout << "Error opening file" << std::endl;
        return 0;
    }

    std::string filename(av[1]);
    filename += ".replace";

    std::ofstream   ofs(filename, std::ios::out);
    if(ofs.is_open()){
        findAndReplace(&buff, s1, s2);
        ofs << buff;
        ofs.close();
    }else{
        std::cout << "Error creating new file" << std::endl;
    }

    return 0;
}