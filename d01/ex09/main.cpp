//
// Created by Artem Iholkin on 10/2/18.
//
#include <iostream>
#include <string>
#include "Logger.hpp"

int     main( void ){
    Logger l("file.log");

    std::string dest_c("cli");
    std::string dest_f("log");
    std::string msg_1("program started");
    std::string msg_2("working 1...");
    std::string msg_3("working 2...");
    std::string msg_4("working 3...");
    std::string msg_5("working 4...");
    std::string msg_6("working 5...");
    std::string msg_7("working 6...");
    std::string msg_8("program ended");

    l.log(dest_f, msg_1);
    l.log(dest_c, msg_1);

    l.log(dest_f, msg_2);
    l.log(dest_c, msg_2);

    l.log(dest_f, msg_3);
    l.log(dest_c, msg_3);

    l.log(dest_f, msg_4);
    l.log(dest_c, msg_4);

    l.log(dest_f, msg_5);
    l.log(dest_c, msg_5);

    l.log(dest_f, msg_6);
    l.log(dest_c, msg_6);

    l.log(dest_f, msg_7);
    l.log(dest_c, msg_7);

    l.log(dest_f, msg_8);
    l.log(dest_c, msg_8);

    return 0;
}