//
// Created by Artem Iholkin on 10/2/18.
//

#ifndef LOGGER_HPP
# define LOGGER_HPP

class Logger{
public:
    Logger( std::string log_file_name );
    ~Logger( void );
    void    log(std::string const & dest, std::string const & message);

private:
    void        _logToConsole(std::string str);
    void        _logToFile(std::string str);
    std::string _makeLogEntry(std::string message);

    std::string _logFileName;
};

#endif
