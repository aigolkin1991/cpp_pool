//
// Created by Artem Iholkin on 10/2/18.
//
#include <iostream>
#include <string>
#include <iomanip>
#include <ctime>
#include <sstream>
#include <fstream>
#include "Logger.hpp"

typedef void (Logger::*method_function)(std::string target);

Logger::Logger( std::string log_file_name ): _logFileName(log_file_name) {
    return ;
};

Logger::~Logger( void ) {
    return ;
}

void        Logger::_logToConsole(std::string str){
    std::cout << str;
    return;
}

void        Logger::_logToFile(std::string str){
    std::ofstream   ofs(this->_logFileName, std::ios::out | std::ios::app);
    if(ofs.is_open()){
        ofs << str;
        ofs.close();
    }else{
        std::cout << "Error creating new file" << std::endl;
    }
}

std::string Logger::_makeLogEntry(std::string message) {
    time_t              timer;
    struct              tm *time_info;
    std::stringstream   ss;
    std::string         result;

    timer = time(NULL);
    time_info = localtime(&timer);
    ss  << "["
        << (time_info->tm_year + 1900)
        << std::setfill('0') << std::setw(2) << time_info->tm_mon
        << std::setfill('0') << std::setw(2) << time_info->tm_mday
        << "_"
        << std::setfill('0') << std::setw(2) << time_info->tm_hour
        << std::setfill('0') << std::setw(2) << time_info->tm_min
        << std::setfill('0') << std::setw(2) << time_info->tm_sec
        << "] "
        << message
        << std::endl;

    result = ss.str();
    return result;
}

void    Logger::log(std::string const & dest, std::string const & message){
    std::string     a_list[] = {
            "cli",
            "log",
    };
    method_function flp[2] = {
            &Logger::_logToConsole,
            &Logger::_logToFile,
    };
    std::string log_message = this->_makeLogEntry(message);

    int i = 0;
    while (i < 2){
        if(a_list[i].compare(dest) == 0){
            method_function func = flp[i];
            (this->*func)(log_message);
            break ;
        }
        i++;
    }
}