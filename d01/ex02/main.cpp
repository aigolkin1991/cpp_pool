#include <iostream>
#include <string>
#include "Zombie.hpp"
#include "ZombieEvent.hpp"

Zombie	*createIndividual(ZombieEvent *ze, std::string name){
	Zombie *new_zombie = ze->newZombie(name);

	return new_zombie;
}

void	meatFactory(ZombieEvent *ze){
	for(int i = 0; i < 10; i++){
		Zombie *z = ze->randomChump();
		delete z;
	}
	return ;
}

void	zombieFactory(){
	ZombieEvent ze;

	ze.setZombieType("brain eater");
	Zombie	*z_1 = createIndividual(&ze, "Patric");

	ze.setZombieType("dummy");
	Zombie	*z_2 = createIndividual(&ze, "Simon");

	ze.setZombieType("scarface");
	Zombie	*z_3 = createIndividual(&ze, "Tyron");

	delete z_3;
	delete z_2;
	delete z_1;

	meatFactory(&ze);
	return ;
}

int main( void ){
	Zombie z("Almost_immortal_Dude", "static");
	std::cout << "Main start" << std::endl;
	zombieFactory();
	std::cout << "Main end" << std::endl;

    	return 0;
}
