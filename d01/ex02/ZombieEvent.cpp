#include <iostream>
#include <string>
#include "Zombie.hpp"
#include "ZombieEvent.hpp"

ZombieEvent::ZombieEvent( void ){
    return ;
}

ZombieEvent::~ZombieEvent( void ){
    return ;
}

std::string     ZombieEvent::getZombieType( void ){
    return this->_type;
}

void            ZombieEvent::setZombieType(std::string type){
    std::cout << "Using type: " << type << std::endl;
    this->_type = type;
}

Zombie          *ZombieEvent::newZombie(std::string name){
    return (new Zombie(this->_type, name));
}

Zombie          *ZombieEvent::randomChump( void ){
    	Zombie *z = new Zombie();
	return z;
}
