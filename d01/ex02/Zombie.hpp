#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP

class   Zombie{
    public:
        Zombie( std::string type, std::string name );
	Zombie( void );
        ~Zombie( void );

        std::string getName( void );
        std::string getType( void );

        void announce( void );

    private:
        std::string _type;
        std::string _name;
};

#endif
