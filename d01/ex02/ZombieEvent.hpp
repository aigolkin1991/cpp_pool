#ifndef ZOMBIE_EVENT_HPP
# define ZOMBIE_EVENT_HPP

class   ZombieEvent{
    public:
        ZombieEvent( void );
        ~ZombieEvent( void );
        void            setZombieType( std::string type);
        std::string     getZombieType( void );
        Zombie          *newZombie(std::string name);
        Zombie          *randomChump( void );

    private:
        std::string     _type;
};

#endif