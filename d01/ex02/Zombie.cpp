#include <iostream>
#include <string>
#include "Zombie.hpp"

Zombie::Zombie( std::string type, std::string name ): _type(type), _name(name)
{
	this->announce();
	return ;
}

Zombie::Zombie( void ){
	std::string random_names[] = {"Bot_crazy", "Bot_Bob", "Bot_salmon", "Bot_bieber_fan", "Bot_Smart"};

	int i = rand() % 5;
	this->_name = random_names[i];
	this->_type = "BOT";

	this->announce();

	return ;
}

Zombie::~Zombie( void ){
    std::cout   << "<" 
                << this->getName()
                << " (" 
                << this->getType() 
                << ")> died" 
                << std::endl;
}

std::string Zombie::getName( void ){
    return this->_name;
}

std::string Zombie::getType( void ){
    return this->_type;
}

void    Zombie::announce( void ){
    std::cout   << "<" 
                << this->getName()
                << " (" 
                << this->getType() 
                << ")> Braiiiiinnnsss" 
                << std::endl;
}
