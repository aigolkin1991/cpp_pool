//
// Created by Artem Iholkin on 10/2/18.
//

#ifndef BRAIN_HPP
# define BRAIN_HPP

class Brain{
public:
    Brain(void);
    ~Brain( void );
    int         getConvolutions( void );
    void        setConvolutions( int convolutions );
    int         getMass( void );
    void        setMass( int mass );
    bool        getIsNecessary( void );
    void        setIsNecessary( bool is_necessary );
    std::string identify( void ) const ;

private:
    int     _convolutions;
    int     _mass;
    bool    _is_necessary;
};

#endif
