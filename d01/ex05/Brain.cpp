//
// Created by Artem Iholkin on 10/2/18.
//

#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include "Brain.hpp"
#include "Human.hpp"

Brain::Brain() {
    return ;
}

Brain::~Brain() {
    return ;
}

int     Brain::getConvolutions( void ){
    return this->_convolutions;
}

void    Brain::setConvolutions( int convolutions ){
    this->_convolutions = convolutions;
}

int     Brain::getMass( void ){
    return this->_mass;
}

void    Brain::setMass( int mass ){
    this->_mass = mass;
}

bool    Brain::getIsNecessary( void ){
    return this->_is_necessary;
}

void    Brain::setIsNecessary( bool is_necessary ){
    this->_is_necessary = is_necessary;
}

std::string Brain::identify( void ) const {
    std::stringstream stream;
    stream << this;
    std::string result( stream.str() );
    return result;
}