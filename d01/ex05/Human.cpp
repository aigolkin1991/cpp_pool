//
// Created by Artem Iholkin on 10/2/18.
//
#include <iostream>
#include <string>
#include "Brain.hpp"
#include "Human.hpp"

Human::Human( void ) {
    return ;
}

Human::~Human( void ) {
    return ;
}

Brain&          Human::getBrain( void ){
    return (Brain&)this->_brain;
}

std::string     Human::identify( void ) const {
    return this->_brain.identify();
}
