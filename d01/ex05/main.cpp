//
// Created by Artem Iholkin on 10/2/18.
//
#include <iostream>
#include <string>
#include "Brain.hpp"
#include "Human.hpp"

int     main( void ){
    Human bob;
    std::cout << bob.identify() << std::endl;
    std::cout << bob.getBrain().identify() << std::endl;

    return 0;
}