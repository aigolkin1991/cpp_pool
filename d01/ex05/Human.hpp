//
// Created by Artem Iholkin on 10/2/18.
//

#ifndef HUMAN_HPP
# define HUMAN_HPP

class Human{
public:
    Human( void );
    ~Human( void );
    std::string     identify( void ) const;
    Brain&          getBrain( void );

private:
    const Brain     _brain;
};

#endif
