//
// Created by Artem Iholkin on 10/2/18.
//
#include <iostream>
#include <string>
#include "Zombie.hpp"
#include "ZombieHorde.hpp"

int main( void ){
    std::cout << "Main start" << std::endl;

    ZombieHorde* zh_1 = new ZombieHorde(23);
    zh_1->announce();
    delete zh_1;

    ZombieHorde* zh_2 = new ZombieHorde(0);
    zh_2->announce();
    delete zh_2;

    std::cout << "Main end" << std::endl;

    return 0;
}
