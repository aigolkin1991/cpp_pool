//
// Created by Artem Iholkin on 10/2/18.
//
#include <iostream>
#include <string>
#include "Zombie.hpp"
#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde( int n ){
    if(n > 0){
        this->_z_list = new Zombie[n];
    }
    else{
        this->_z_list = NULL;
    }
    this->_z_list_len = n;
    return ;
}

ZombieHorde::~ZombieHorde( void ){
    if(this->_z_list != NULL){
        delete [] this->_z_list;
        std::cout << "All " << this->_z_list_len << " zombies are dead" << std::endl;
    }else{
        std::cout << "No zombies - no problems" << std::endl;
    }

    return ;
}

void    ZombieHorde::announce( void ){
    for(int i = 0; i < this->_z_list_len; i++){
        this->_z_list[i].announce();
    }
}

