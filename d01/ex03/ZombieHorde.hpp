//
// Created by Artem Iholkin on 10/2/18.
//

#ifndef ZOMBIEHORDE_HPP
# define ZOMBIEHORDE_HPP

class ZombieHorde{
public:
    ZombieHorde( int n );
    ~ZombieHorde( void );
    void    announce( void );

private:
    Zombie* _z_list;
    int     _z_list_len;
};

#endif