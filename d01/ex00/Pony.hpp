#ifndef PONY_HPP
# define PONY_HPP

class Pony{
public:
    Pony( std::string name, std::string color );
    ~Pony( void );

    std::string getName( void );
    std::string getColor( void );
    int         getAge( void );
    int         getHeight( void );
    int         getMinSpeed( void );
    int         getMaxSpeed( void );
    void        say( void );
    
private:
    std::string     _name;
    std::string     _color;
    int       _age;
    int       _height;
    int       _min_speed;
    int       _max_speed;
};

#endif