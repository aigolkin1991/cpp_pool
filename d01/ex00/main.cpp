#include <iostream>
#include <string>
#include "Pony.hpp"

void    ponyOnTheHeap( void ){
    std::string name("Burka_on_the_heap");
    std::string color("brown");

    Pony* myLittlePony = new Pony(name, color);
    myLittlePony->say();
    delete myLittlePony;
    return ;
}

void    ponyOnTheStack( void ){
    std::string name("Sivka_on_the_stack");
    std::string color("white");

    Pony myLittlePony(name, color);
    myLittlePony.say();
    return ;
}

int main( void ){
    std::cout << "Start main" << std::endl;
    ponyOnTheHeap();
    ponyOnTheStack();
    std::cout << "End main" << std::endl;

    return 0;
}