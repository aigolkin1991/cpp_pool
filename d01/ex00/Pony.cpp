#include <iostream>
#include <string>
#include "Pony.hpp"

Pony::Pony( std::string name, std::string color ) : _name(name), _color(color)
{
    this->_age = 1;
    this->_height = 2;
    this->_min_speed = 7;
    this->_max_speed  =32;
    std::cout   << "Here is your new pony"
                << std::endl
                << "Here name is "
                << name
                << std::endl
                << "She is "
                << this->_age
                << " years old"
                << " and "
                << this->_height
                << " meters tall."
                << std::endl
                << "When she walks, here speed is "
                << this->_min_speed
                << " and when run - "
                << this->_max_speed
                << std::endl;
}

Pony::~Pony( void ){
    std::cout << "Your pony " << this->_name << " is dead. How sad" << std::endl;
}

std::string Pony::getName( void ){
    return this->_name;
}

std::string Pony::getColor( void ){
    return this->_color;
}

int Pony::getAge( void ){
    return this->_age;
}

int Pony::getHeight( void ){
    return this->_height;
}

int Pony::getMinSpeed( void ){
    return this->_min_speed;
}

int Pony::getMaxSpeed( void ){
    return this->_max_speed;
}

void    Pony::say( void ){
    std::cout << "Pony " << this->_name << " says " << "Pfffrrr...." << std::endl;
    return ;
}