//
// Created by Artem Iholkin on 10/2/18.
//
#include <iostream>
#include <string>
#include "Human.hpp"

int main( void ){
    Human h;

    std::string     action_1 = "mA";
    std::string     target_1 = "innocent tree";

    std::string     action_2 = "rA";
    std::string     target_2 = "crazy beaver";

    std::string     action_3 = "iS";
    std::string     target_3 = "your neighbor";

    h.action(action_1, target_1);
    h.action(action_2, target_2);
    h.action(action_3, target_3);
    return 0;
}