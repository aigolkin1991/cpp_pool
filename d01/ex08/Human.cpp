//
// Created by Artem Iholkin on 10/2/18.
//
#include <iostream>
#include <string>
#include "Human.hpp"

typedef void (Human::*method_function)(std::string const & target);

void Human::meleeAttack(std::string const & target){
    std::cout << "Atacking " << target << " with bare hands" << std::endl;
}

void Human::rangedAttack(std::string const & target){
    std::cout << "Shoot an eye of " << target << " with elven crossbow" << std::endl;
}

void Human::intimidatingShout(std::string const & target){
    std::cout << "Blow up the head of " << target << " with abusive language" << std::endl;
}

void Human::action(std::string const & action_name, std::string const & target){
    std::string     a_list[] = {
            "mA",
            "rA",
            "iS",
    };

    method_function flp[3] = {
            &Human::meleeAttack,
            &Human::rangedAttack,
            &Human::intimidatingShout
    };

    int i = 0;
    while (i < 3){
        if(a_list[i].compare(action_name) == 0){
            method_function func = flp[i];
            (this->*func)(target);
            break ;
        }
        i++;
    }
}