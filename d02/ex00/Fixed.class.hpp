#ifndef FIXED_HPP
# define FIXED_HPP

class	Fixed{
public:
	Fixed( void );
	Fixed( int fpv );
	Fixed( const Fixed &src );
	~Fixed( void );
	int			getRawBits( void ) const;
	void			setRawBits( int const new_fpv );
	Fixed 			&operator=( const Fixed &src );

private:
	int			_fixedPointValue;
	static const int	_nb_of_bits;
};

#endif
