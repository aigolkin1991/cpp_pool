#include <iostream>
#include <string>
#include "Fixed.class.hpp"

const int	_nb_of_bits = 8;

Fixed::Fixed( void ):_fixedPointValue(0){
	if(_nb_of_bits){}
	std::cout << "Default constructor called" << std::endl;
	return ;
}

Fixed::Fixed( int fpv ):_fixedPointValue(fpv){
	std::cout << "Parametric constructor called" << std::endl;
	return ;
}

Fixed::Fixed( const Fixed &src ){
	std::cout << "Copy constructor called" << std::endl;
	*this = src;
	return ;
}

Fixed::~Fixed( void ){
	std::cout << "Destructor called" << std::endl;
	return ;
}

int	Fixed::getRawBits( void ) const {
	std::cout << "getRawBits member function called" << std::endl;
	return this->_fixedPointValue;
}

void	Fixed::setRawBits( int const new_fpv ){
	std::cout << "setRawBits member function called" << std::endl;
	this->_fixedPointValue = new_fpv;
	return ;
}

Fixed	&Fixed::operator=(const Fixed &src)
{
	if(this != &src){
		this->_fixedPointValue = src.getRawBits();
	}
	std::cout << "Assignation operator called" << std::endl;
	return (*this);
}
