#include "Fixed.class.hpp"

const int	Fixed::_nb_of_bits = 8;

Fixed::Fixed( void ):_fixedPointValue(0){
	std::cout << "Default constructor called" << std::endl;
	return ;
}

Fixed::Fixed( const int fpv ){
	this->_fixedPointValue = fpv << Fixed::_nb_of_bits;
	std::cout << "Parametric (int) constructor called" << std::endl;
	return ;
}

Fixed::Fixed( const float fpv ){
	this->_fixedPointValue = roundf(fpv * (1 << Fixed::_nb_of_bits));
	std::cout << "Parametric (float) constructor called" << std::endl;
	return ;
}

Fixed::Fixed( const Fixed &src ){
	std::cout << "Copy constructor called" << std::endl;
	*this = src;
	return ;
}

Fixed::~Fixed( void ){
	std::cout << "Destructor called" << std::endl;
	return ;
}

int	Fixed::getRawBits( void ) const {
	return this->_fixedPointValue;
}

void	Fixed::setRawBits( int const new_fpv ){
	this->_fixedPointValue = new_fpv;
	return ;
}

float           Fixed::toFloat( void ) const{
	return ((float)(this->_fixedPointValue) / (1 << _nb_of_bits));
}

int             Fixed::toInt( void ) const{
	return ((int)(this->_fixedPointValue >> _nb_of_bits));
}

Fixed			&Fixed::operator=(const Fixed &src) {
	if(this != &src){
		this->_fixedPointValue = src.getRawBits();
	}
	std::cout << "Assignation operator called" << std::endl;
	return (*this);
}

std::ostream	&operator<<(std::ostream &out, Fixed const &val) {
	out << val.toFloat();
	return (out);
}