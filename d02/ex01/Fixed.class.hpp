#ifndef FIXED_HPP
# define FIXED_HPP

#include <iostream>
#include <string>
#include <cmath>

class	Fixed{
public:
	Fixed( void );
	Fixed( const int fpv );
	Fixed( const float fpv );
	Fixed( const Fixed &src );
	~Fixed( void );
	int			        getRawBits( void ) const;
	void			    setRawBits( int const new_fpv );
    float               toFloat( void ) const;
    int                 toInt( void ) const;
	Fixed 		    	&operator=( const Fixed &src );

private:
	int					_fixedPointValue;
	static const int	_nb_of_bits;
};

std::ostream 	        &operator<<( std::ostream &out, Fixed const &val );

#endif
