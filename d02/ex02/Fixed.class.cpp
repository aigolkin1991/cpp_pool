#include "Fixed.class.hpp"

const int	Fixed::_nb_of_bits = 8;

Fixed::Fixed( void ):_fixedPointValue(0){
	return ;
}

Fixed::Fixed( const int fpv ){
	this->_fixedPointValue = fpv << Fixed::_nb_of_bits;
	return ;
}

Fixed::Fixed( const float fpv ){
	this->_fixedPointValue = roundf(fpv * (1 << Fixed::_nb_of_bits));
	return ;
}

Fixed::Fixed( const Fixed &src ){
	*this = src;
	return ;
}

Fixed::~Fixed( void ){
	return ;
}

int	Fixed::getRawBits( void ) const {
	return this->_fixedPointValue;
}

void	Fixed::setRawBits( int const new_fpv ){
	this->_fixedPointValue = new_fpv;
	return ;
}

float           Fixed::toFloat( void ) const{
	return ((float)(this->_fixedPointValue) / (1 << _nb_of_bits));
}

int             Fixed::toInt( void ) const{
	return ((int)(this->_fixedPointValue >> _nb_of_bits));
}

Fixed			&Fixed::operator=(const Fixed &src) {
	if(this != &src){
		this->_fixedPointValue = src.getRawBits();
	}
	return (*this);
}

Fixed				Fixed::operator+(const Fixed &src ) const {
	return Fixed(this->toFloat() + src.toFloat());
}

Fixed				Fixed::operator-(const Fixed &src ) const {
	return Fixed(this->toFloat() - src.toFloat());
}

Fixed				Fixed::operator*(const Fixed &src ) const {
	return Fixed(this->toFloat() * src.toFloat());
}

Fixed				Fixed::operator/(const Fixed &src ) const {
	return Fixed(this->toFloat() / src.toFloat());
}

Fixed&				Fixed::operator++( void ){
	this->setRawBits((this->getRawBits()+1));
	return (*this);
}

Fixed&				Fixed::operator--( void ){
	this->setRawBits((this->getRawBits()-1));
	return (*this);
}

Fixed&				Fixed::operator++( int ){
	Fixed*	copy = new Fixed(*this);
	++(*this);

	return (*copy);
}

Fixed&				Fixed::operator--( int ){
	Fixed*	copy = new Fixed(*this);
	--(*this);

	return (*copy);
}

bool                Fixed::operator<( const Fixed &src ) const {
	return this->getRawBits() < src.getRawBits();
}

bool                Fixed::operator>( const Fixed &src ) const {
	return this->getRawBits() > src.getRawBits();
}

bool                Fixed::operator<=( const Fixed &src ) const {
	return this->getRawBits() <= src.getRawBits();
}

bool                Fixed::operator>=( const Fixed &src ) const {
	return this->getRawBits() >= src.getRawBits();
}

bool                Fixed::operator!=( const Fixed &src ) const {
	return this->getRawBits() != src.getRawBits();
}

Fixed&				Fixed::min(Fixed &n1, Fixed &n2){
	return (n1 < n2) ? n1 : n2;
}

Fixed&				Fixed::max(Fixed &n1, Fixed &n2){
	return (n1 > n2) ? n1 : n2;
}

const Fixed&		Fixed::min(const Fixed& n1, const Fixed& n2){
	return (n1 < n2) ? n1 : n2;
}

const Fixed&		Fixed::max(const Fixed& n1, const Fixed& n2){
	return (n1 > n2) ? n1 : n2;
}

std::ostream	&operator<<(std::ostream &out, Fixed const &val) {
	out << val.toFloat();
	return (out);
}