#ifndef FIXED_HPP
# define FIXED_HPP

#include <iostream>
#include <string>
#include <cmath>

class	Fixed{
public:
	Fixed( void );
	Fixed( const int fpv );
	Fixed( const float fpv );
	Fixed( const Fixed &src );
	~Fixed( void );
	int			        getRawBits( void ) const;
	void			    setRawBits( int const new_fpv );
    float               toFloat( void ) const;
    int                 toInt( void ) const;
	Fixed 		    	&operator=( const Fixed &src );
	Fixed				operator+(const Fixed &src ) const ;
	Fixed				operator-(const Fixed &src ) const ;
	Fixed				operator*(const Fixed &src ) const ;
	Fixed				operator/(const Fixed &src ) const ;

	Fixed&				operator++( void );
	Fixed&				operator--( void );
	Fixed&				operator++( int );
	Fixed&				operator--( int );

	bool                operator<( const Fixed &src ) const ;
    bool                operator>( const Fixed &src ) const ;
    bool                operator<=( const Fixed &src ) const ;
    bool                operator>=( const Fixed &src ) const ;
    bool                operator!=( const Fixed &src ) const ;

    static Fixed&			min(Fixed &n1, Fixed &n2);
	static Fixed&			max(Fixed &n1, Fixed &n2);
	const static Fixed&		min(const Fixed& n1, const Fixed& n2);
	const static Fixed&		max(const Fixed& n1, const Fixed& n2);


private:
	int					_fixedPointValue;
	static const int	_nb_of_bits;
};

std::ostream 	        &operator<<( std::ostream &out, Fixed const &val );

#endif
