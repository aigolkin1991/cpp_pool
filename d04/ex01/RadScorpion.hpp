//
// Created by Artem Iholkin on 10/5/18.
//

#ifndef D04_RADSCORPION_HPP
#define D04_RADSCORPION_HPP

#include "Enemy.hpp"

class RadScorpion : public Enemy {

public:
    RadScorpion();

    ~RadScorpion();

    RadScorpion &operator=(RadScorpion const &rhs);

//    void takeDamage(int amount);
};

#endif //D04_RADSCORPION_HPP
