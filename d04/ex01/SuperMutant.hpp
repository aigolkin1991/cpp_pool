//
// Created by Artem Iholkin on 10/5/18.
//

#ifndef D04_SUPERMUTANT_HPP
#define D04_SUPERMUTANT_HPP

#include "Enemy.hpp"

class SuperMutant : public Enemy {

public:
    SuperMutant();

    ~SuperMutant();

    SuperMutant &operator=(SuperMutant const &rhs);

    void takeDamage(int amount);
};

#endif //D04_SUPERMUTANT_HPP
