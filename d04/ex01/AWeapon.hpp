//
// Created by Artem Iholkin on 10/5/18.
//

#ifndef D04_AWEAPON_HPP
#define D04_AWEAPON_HPP

#include <iostream>

class AWeapon {
public:
    AWeapon(const std::string &name, int apcost, int damage);

    virtual ~AWeapon();

    std::string getName() const;

    int getAPCost() const;

    int getDamage() const;

    AWeapon &operator=(AWeapon const &rhs);

    virtual void attack() const = 0;

protected:
    std::string name;
    int damage;
    int APcost;

    AWeapon();
};

#endif //D04_AWEAPON_HPP
