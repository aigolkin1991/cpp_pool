//
// Created by Artem Iholkin on 10/5/18.
//

#ifndef D04_PLASMARIFLE_HPP
#define D04_PLASMARIFLE_HPP


#include "AWeapon.hpp"

class PlasmaRifle : public AWeapon {
public:
    PlasmaRifle();

    ~PlasmaRifle();

    PlasmaRifle &operator=(PlasmaRifle const &rhs);

    void attack() const;

private:

};


#endif //D04_PLASMARIFLE_HPP
