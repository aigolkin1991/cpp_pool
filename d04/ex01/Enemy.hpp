//
// Created by Artem Iholkin on 10/5/18.
//

#ifndef D04_ENEMY_HPP
#define D04_ENEMY_HPP

#include <iostream>

class Enemy {
protected:
    int hp;
    std::string type;

public:
    Enemy(int hp, std::string const &type);

    virtual ~Enemy();

    std::string getType() const;

    int getHP() const;

    void setHP(int amount);

    Enemy &operator=(Enemy const &rhs);

    virtual void takeDamage(int amount);
};


#endif //D04_ENEMY_HPP
