//
// Created by Artem Iholkin on 10/5/18.
//

#ifndef D04_POWERFIST_HPP
#define D04_POWERFIST_HPP

#include "AWeapon.hpp"

class PowerFist : public AWeapon {
public:
    PowerFist();

    ~PowerFist();

    PowerFist &operator=(PowerFist const &rhs);

    void attack() const;

private:

};

#endif //D04_POWERFIST_HPP
