//
// Created by Artem Iholkin on 10/5/18.
//

#include "Peon.hpp"

Peon::Peon() {
    return ;
}

Peon::Peon(std::string name) : Victim(name) {
    std::cout << "Zog zog." << std::endl;
}

Peon::Peon(const Peon& src) {
    *this = src;
    return ;
}

Peon::~Peon() {
    std::cout << "Bleuark..." << std::endl;
}

void Peon::getPolymorphed() const {
    std::cout << this->getName() << " has been turned into a pink Pony !" << std::endl;

}

Peon &Peon::operator=(Peon const &rhs) {
    this->_name = rhs.getName();
    return *this;
}