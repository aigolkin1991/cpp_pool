//
// Created by Artem Iholkin on 10/5/18.
//

#ifndef D04_PEON_HPP
#define D04_PEON_HPP

#include <iostream>
#include "Victim.hpp"

class Peon : public Victim {

public:

    Peon(std::string name);

    Peon(const Peon &src);

    Peon &operator=(Peon const &rhs);

    ~Peon();

    void getPolymorphed() const;

private :

    Peon();
};

#endif //D04_PEON_HPP
