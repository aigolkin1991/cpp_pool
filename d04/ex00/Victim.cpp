//
// Created by Artem Iholkin on 10/5/18.
//
#include "Victim.hpp"

Victim::Victim( void ) {
    return ;
}

Victim::Victim(std::string name): _name(name) {
    std::cout << "Some random victim called " << this->_name << " just popped !" << std::endl;
    return ;
}

Victim::Victim(const Victim& src ) {
    *this = src;
    return ;
}

Victim::~Victim( void ) {
    std::cout << "Victim " << this->_name << " just died for no apparent reason !" << std::endl;
}

std::ostream &operator<<(std::ostream &o, const Victim &rhs) {
    o << "I'm " << rhs.getName() << " and I like otters !" << std::endl;
    return o;
}

Victim &Victim::operator=(Victim const &rhs) {
    this->_name = rhs.getName();
    return *this;
}

std::string     Victim::getName() const{
    return this->_name;
}

void            Victim::setName(std::string name){
    this->_name = name;
}

void Victim::getPolymorphed() const {
    std::cout << this->getName() << " has been turned into a cute little sheep !" << std::endl;

}

