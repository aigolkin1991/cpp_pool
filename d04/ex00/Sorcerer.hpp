//
// Created by Artem Iholkin on 10/5/18.
//

#ifndef D04_SORCERER_HPP
#define D04_SORCERER_HPP

#include <iostream>
#include "Victim.hpp"

class Sorcerer {

public:

    Sorcerer(std::string name, std::string title);

    Sorcerer(const Sorcerer &src);

    void polymorph(Victim const &src);

    std::string getName() const;

    void setName(std::string);

    Sorcerer &operator=(Sorcerer const &rhs);

    std::string getTitle() const;

    void setTitle(std::string);

    ~Sorcerer();

private :
    Sorcerer();

    std::string _name;
    std::string _title;
};

std::ostream &operator<<(std::ostream &o, const Sorcerer &i);

#endif //D04_SORCERER_HPP
