//
// Created by Artem Iholkin on 10/5/18.
//

#ifndef D04_VICTIM_HPP
#define D04_VICTIM_HPP

#include <iostream>

class Victim {

public:

    Victim(std::string name);

    Victim(const Victim &src);

    virtual ~Victim();

    std::string getName() const;

    Victim &operator=(Victim const &rhs);

    virtual void getPolymorphed() const;

    void setName(std::string name);

protected :

    Victim();

    std::string _name;
};

std::ostream &operator<<(std::ostream &o, const Victim &i);

#endif //D04_VICTIM_HPP
