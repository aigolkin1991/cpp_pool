//
// Created by Artem Iholkin on 10/5/18.
//

#include "Sorcerer.hpp"

Sorcerer::Sorcerer( void ) {
    return ;
}

Sorcerer::Sorcerer(std::string name, std::string title): _name(name), _title(title) {
    std::cout << this->_name << ", " << this->_title << ", is born" << std::endl;

    return ;
}

Sorcerer::Sorcerer(const Sorcerer& src ) {
    *this = src;
    return ;
}

Sorcerer::~Sorcerer( void ) {
    std::cout << this->_name <<", " << this->_title << ", is dead. Consequences will never be the same !" << std::endl;
}

std::ostream &operator<<(std::ostream &o, const Sorcerer &rhs) {
    o << "I am " << rhs.getName() << ", " << rhs.getTitle() << ", and I like ponies !" << std::endl;
    return o;
}

Sorcerer &Sorcerer::operator=(Sorcerer const &rhs) {
    this->_name = rhs.getName();
    this->_title = rhs.getTitle();
    return *this;
}

std::string     Sorcerer::getTitle() const{
    return this->_title;
}

void            Sorcerer::setTitle(std::string title){
    this->_title = title;
}

std::string     Sorcerer::getName() const{
    return this->_name;
}

void            Sorcerer::setName(std::string name){
    this->_name = name;
}

void Sorcerer::polymorph(const Victim &src) {
    src.getPolymorphed();
}