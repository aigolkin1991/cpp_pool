//
// Created by Artem Iholkin on 10/3/18.
//
#include "FragTrap.hpp"

typedef void (FragTrap::*method_function)(std::string const & target) const ;

FragTrap::FragTrap( void ){
    return ;
}

FragTrap::FragTrap( std::string name ) :
_name(name),
_hp(100),
_max_hp(100),
_ep(100),
_max_ep(100),
_lvl(1),
_melee_dmg(30),
_range_dmg(20),
_armor_dmg_reduction(3),
_pistol_dmg(18),
_shotgun_dmg(22),
_fire_dmg(22),
_corrosive_dmg(28),
_cryo_dmg(14)
{
    std::cout << "FragTrap [" << name << "] was created" << std::endl;
    std::cout << "Directive one: Protect humanity! Directive two: Obey Jack at all costs. Directive three: Dance!" << std::endl;
    return ;
}

FragTrap::FragTrap( const FragTrap& val){
    std::cout << "FragTrap [" << val.getName() << "] copy was created" << std::endl;
    *this = val;
    return ;
}

FragTrap::~FragTrap( void ){
    std::cout << "FragTrap [" << this->getName() << "] was destroyed" << std::endl;
    return ;
}

FragTrap&   FragTrap::operator=( const FragTrap& val){
    if(this != &val){
        this->setName(val.getName());
        this->setMaxHp(val.getMaxHp());
        this->setMaxEp(val.getMaxEp());
        this->setHp(val.getHp());
        this->setEp(val.getEp());
        this->setLvl(val.getLvl());
        this->setMeleeDmg(val.getMeleeDmg());
        this->setRangedDmg(val.getRangedDmg());
        this->setArmorDmgReduction(val.getArmorDmgReduction());
    }
    std::cout << "Assignation operator called" << std::endl;
    return (*this);
}


unsigned int    FragTrap::getHp( void ) const {
    return this->_hp;
}

void            FragTrap::setHp( unsigned int val ){
    if(val <= this->getMaxHp()){
        this->_hp = val;
    }
}

unsigned int    FragTrap::getMaxHp( void ) const {
    return this->_max_hp;
}

void            FragTrap::setMaxHp( unsigned int val ){
    this->_max_hp = val;
}

unsigned int    FragTrap::getEp( void ) const {
    return this->_ep;
}

void            FragTrap::setEp(unsigned int val ){
    if(val <= this->getMaxEp()){
        this->_ep = val;
    }
}

unsigned int    FragTrap::getMaxEp( void ) const {
    return this->_max_ep;
}

void            FragTrap::setMaxEp( unsigned int val ){
    this->_max_ep = val;
}

unsigned int    FragTrap::getLvl( void ) const {
    return this->_lvl;
}

void            FragTrap::setLvl( unsigned int val ){
    if(val >= 1){
        this->_lvl = val;
    }
}

unsigned int    FragTrap::getMeleeDmg( void ) const {
    return this->_melee_dmg;
}

void            FragTrap::setMeleeDmg( unsigned int val ){
    this->_melee_dmg = val;
}

unsigned int    FragTrap::getRangedDmg( void ) const {
    return this->_range_dmg;
}

void            FragTrap::setRangedDmg( unsigned int val ){
    this->_range_dmg = val;
}

unsigned int    FragTrap::getPistolDmg( void ) const {
    return this->_pistol_dmg;
}

void            FragTrap::setPistolDmg( unsigned int val ){
    if(val > 0){
        this->_pistol_dmg = val;
    }
}

unsigned int    FragTrap::getShotGunDmg( void ) const {
    return this->_shotgun_dmg;
}

void            FragTrap::setShotGunDmg( unsigned int val ){
    if(val > 0){
        this->_shotgun_dmg = val;
    }
}

unsigned int    FragTrap::getCorrosiveDmg( void ) const {
    return this->_corrosive_dmg;
}

void            FragTrap::setCorrosiveDmg( unsigned int val ){
    if(val > 0){
        this->_corrosive_dmg = val;
    }
}

unsigned int    FragTrap::getCryoDmg( void ) const {
    return this->_cryo_dmg;
}

void            FragTrap::setCryoDmg( unsigned int val ){
    if(val > 0){
        this->_cryo_dmg = val;
    }
}

unsigned int    FragTrap::getFireDmg( void ) const {
    return this->_fire_dmg;
}

void            FragTrap::setFireDmg( unsigned int val ){
    if(val > 0){
        this->_fire_dmg = val;
    }
}

void            FragTrap::pistolAttack(std::string const &target) const {
    std::cout << "I'd do anything for a woman with a gun." << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with pistol dealing " << this->getPistolDmg() << " points of damage]" << std::endl;
}

void            FragTrap::shotgunAttack(std::string const &target) const {
    std::cout << "In yo' FACE!" << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with shotgun dealing " << this->getShotGunDmg() << " points of damage]" << std::endl;
}

void            FragTrap::fireAttack(std::string const &target) const {
    std::cout << "I am Fire, I am Death!" << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with firegun dealing " << this->getFireDmg() << " points of damage]" << std::endl;
}

void            FragTrap::corrosiveAttack(std::string const &target) const {
    std::cout << "Time to melt some faces." << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with acid dealing " << this->getCorrosiveDmg() << " points of damage]" << std::endl;
}

void            FragTrap::cryoAttack(std::string const &target) const {
    std::cout << "Know what killed the baddies? The Ice age." << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with icegun dealing " << this->getCryoDmg() << " points of damage]" << std::endl;
}

unsigned int    FragTrap::getArmorDmgReduction( void ) const {
    return this->_armor_dmg_reduction;
}

void            FragTrap::setArmorDmgReduction( unsigned int val ){
    this->_armor_dmg_reduction = val;
}

std::string     FragTrap::getName( void ) const {
    return this->_name;
}

void            FragTrap::setName( std::string new_name ){
    if(new_name.size() > 0)
        this->_name = new_name;
}


void            FragTrap::rangedAttack(std::string const & target){
    std::cout << "FR4G-TP " << this->getName() << " attacks " << target << " at range, causing " << this->getRangedDmg() << " points of damage !" << std::endl;
}

void            FragTrap::meleeAttack(std::string const & target){
    std::cout << "FR4G-TP " << this->getName() << " attacks " << target << " with manipulator, causing " << this->getMeleeDmg() << " points of damage !" << std::endl;
}

void            FragTrap::takeDamage(unsigned int amount){
    unsigned int    reduced = 0;

    if(this->getHp() == 0){
        std::cout   << "FR4G-TP " << this->getName() << " completely dead. Does this make you feel like man - attacking hepless heap of rusty metal" << std::endl;
        return;
    }

    if(amount > this->getArmorDmgReduction())
        reduced = (amount - this->getArmorDmgReduction());

    if( reduced >= this->getHp() ){
        std::cout   << "FR4G-TP " << this->getName()
                    << " takes " << amount << " points of damage, shield reduces demage to "
                    << reduced << " , but it won't help " << this->getName() << " became destroyed" << std::endl;
        this->setHp(0);
    }else{
        std::cout << "FR4G-TP " << this->getName()
                  << " takes " << amount << " points of damage, shield reduces demage to "
                  << reduced << ". Healt points left: " << (this->getHp() - reduced) << std::endl;
        this->setHp(this->getHp() - reduced);
    }
}

void            FragTrap::beRepaired(unsigned int amount){
    if(this->getEp() == 0){
        std::cout << "FR4G-TP have no energy to repair" << std::endl;
        return;
    }

    unsigned int    allowedAmount = this->getEp() >= amount ? amount : this->getEp();
    unsigned int    resultingHp = allowedAmount + this->getHp();

    if(this->getMaxHp() == this->getHp()){
        std::cout   << "FR4G-TP " << this->getName() << " doesn't require repair" << std::endl;
        return;
    }else if(this->getHp() == 0){
        std::cout   << "FR4G-TP " << this->getName() << " completely dead and can't be repaired" << std::endl;
        return;
    }

    if(resultingHp >= this->getMaxHp()){
        std::cout   << "FR4G-TP " << this->getName() << " has been fully repaired, and now have " << this->getMaxHp() << " health points" << std::endl;
        this->setEp(this->getEp() - (this->getMaxHp() - this->getHp()));
        this->setHp(this->getMaxHp());
    }else{
        std::cout   << "FR4G-TP " << this->getName() << " has restored " << allowedAmount << " health points, and now have " << resultingHp << " health points" << std::endl;
        this->setHp(resultingHp);
        this->setEp(this->getEp() - allowedAmount);
    }
}

void            FragTrap::vaulthunter_dot_exe(std::string const & target){
    if(this->getEp() < 25){
        std::cout << "Not enough energy points" << std::endl;
    }else{

        method_function flp[5] = {
                &FragTrap::pistolAttack,
                &FragTrap::shotgunAttack,
                &FragTrap::fireAttack,
                &FragTrap::corrosiveAttack,
                &FragTrap::cryoAttack
        };

        std::cout << "Look out everybody, things are about to get awesome!" << std::endl;
        int i = 5;
        int attackNb = 0;
        while (i--){
            srand(time( 0 ) + i);
            attackNb = (rand() % 5);
            method_function func = flp[attackNb];
            (this->*func)(target);
        }
        this->setEp(this->getEp() - 25);
    }
}

std::ostream	&operator<<(std::ostream &out, FragTrap const &val) {
    out << std::setw(13) << "Name: " << val.getName() << std::endl
        << std::setw(13) << "Level: " << val.getLvl() << std::endl
        << std::setw(13) << "Hp: " << val.getHp() << "/" << val.getMaxHp() << std::endl
        << std::setw(13) << "Ep: " << val.getEp() << "/" << val.getMaxEp() << std::endl
        << std::setw(13) << "Melee atck: " << val.getMeleeDmg() << std::endl
        << std::setw(13) << "Ranged atck: " << val.getRangedDmg() << std::endl
        << std::setw(13) << "Armor: " << val.getArmorDmgReduction() << std::endl;
    return (out);
}