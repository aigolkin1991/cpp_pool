//
// Created by Artem Iholkin on 10/3/18.
//
#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include "FragTrap.hpp"

int     main( void ){
    std::string name = "First_bot";

    FragTrap    bot_1(name);

//    bot_1.vaulthunter_dot_exe("Beaver");
    bot_1.takeDamage(20);
    bot_1.takeDamage(50);

    bot_1.beRepaired(50);

    bot_1.takeDamage(20);
    bot_1.takeDamage(20);

    bot_1.beRepaired(50);
    bot_1.beRepaired(50);
    bot_1.beRepaired(50);
    bot_1.takeDamage(6);
    bot_1.beRepaired(50);
    std::cout << bot_1;


    return 0;
}