//
// Created by Artem Iholkin on 10/4/18.
//

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

#include <iostream>
#include <string>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
#include <ctime>

class ScavTrap{
public:
    ScavTrap( std::string name );
    ScavTrap( const ScavTrap& val );
    ~ScavTrap( void );

    ScavTrap&       operator=( const ScavTrap& val );

    unsigned int    getHp( void ) const ;
    void            setHp( unsigned int val );

    unsigned int    getMaxHp( void ) const ;
    void            setMaxHp( unsigned int val );

    unsigned int    getEp( void ) const ;
    void            setEp(unsigned int val );

    unsigned int    getMaxEp( void ) const ;
    void            setMaxEp( unsigned int val );

    unsigned int    getLvl( void ) const ;
    void            setLvl( unsigned int val );

    unsigned int    getMeleeDmg( void ) const ;
    void            setMeleeDmg( unsigned int val );

    unsigned int    getRangedDmg( void ) const ;
    void            setRangedDmg( unsigned int val );

    unsigned int    getPistolDmg( void ) const ;
    void            setPistolDmg( unsigned int val );

    unsigned int    getShotGunDmg( void ) const ;
    void            setShotGunDmg( unsigned int val );

    unsigned int    getCorrosiveDmg( void ) const ;
    void            setCorrosiveDmg( unsigned int val );

    unsigned int    getCryoDmg( void ) const ;
    void            setCryoDmg( unsigned int val );

    unsigned int    getFireDmg( void ) const ;
    void            setFireDmg( unsigned int val );

    unsigned int    getArmorDmgReduction( void ) const ;
    void            setArmorDmgReduction( unsigned int val );

    std::string     getName( void ) const ;
    void            setName( std::string new_name );

    void            rangedAttack(std::string const & target);
    void            meleeAttack(std::string const & target);
    void            takeDamage(unsigned int amount);
    void            beRepaired(unsigned int amount);

    //Attacks
    void            pistolAttack(std::string const &target) const ;
    void            shotgunAttack(std::string const &target) const ;
    void            fireAttack(std::string const &target) const ;
    void            corrosiveAttack(std::string const &target) const ;
    void            cryoAttack(std::string const &target) const ;

    //Chalanges
    void            challenge_1( void ) const ;
    void            challenge_2( void ) const ;
    void            challenge_3( void ) const ;
    void            challenge_4( void ) const ;
    void            challenge_5( void ) const ;

    void            challengeNewcomer( std::string const &target ) const ;

private:
    ScavTrap( void );
    std::string     _name;
    unsigned int    _hp, _max_hp, _ep, _max_ep, _lvl, _melee_dmg,
    _range_dmg, _armor_dmg_reduction, _pistol_dmg, _shotgun_dmg, _fire_dmg, _corrosive_dmg, _cryo_dmg;
};

std::ostream        &operator<<( std::ostream &out, ScavTrap const &val );

#endif