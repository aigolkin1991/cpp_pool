//
// Created by Artem Iholkin on 10/3/18.
//

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

#include <iostream>
#include <string>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
#include <ctime>

class FragTrap{
public:
    FragTrap( std::string name );
    FragTrap( const FragTrap& val );
    ~FragTrap( void );

    FragTrap&       operator=( const FragTrap& val );

    unsigned int    getHp( void ) const ;
    void            setHp( unsigned int val );

    unsigned int    getMaxHp( void ) const ;
    void            setMaxHp( unsigned int val );

    unsigned int    getEp( void ) const ;
    void            setEp(unsigned int val );

    unsigned int    getMaxEp( void ) const ;
    void            setMaxEp( unsigned int val );

    unsigned int    getLvl( void ) const ;
    void            setLvl( unsigned int val );

    unsigned int    getMeleeDmg( void ) const ;
    void            setMeleeDmg( unsigned int val );

    unsigned int    getRangedDmg( void ) const ;
    void            setRangedDmg( unsigned int val );

    unsigned int    getPistolDmg( void ) const ;
    void            setPistolDmg( unsigned int val );

    unsigned int    getShotGunDmg( void ) const ;
    void            setShotGunDmg( unsigned int val );

    unsigned int    getCorrosiveDmg( void ) const ;
    void            setCorrosiveDmg( unsigned int val );

    unsigned int    getCryoDmg( void ) const ;
    void            setCryoDmg( unsigned int val );

    unsigned int    getFireDmg( void ) const ;
    void            setFireDmg( unsigned int val );

    unsigned int    getArmorDmgReduction( void ) const ;
    void            setArmorDmgReduction( unsigned int val );



    std::string     getName( void ) const ;
    void            setName( std::string new_name );

    void            rangedAttack(std::string const & target);
    void            meleeAttack(std::string const & target);
    void            takeDamage(unsigned int amount);
    void            beRepaired(unsigned int amount);

    void            vaulthunter_dot_exe(std::string const & target);

    //Attacks
    void            pistolAttack(std::string const &target) const ;
    void            shotgunAttack(std::string const &target) const ;
    void            fireAttack(std::string const &target) const ;
    void            corrosiveAttack(std::string const &target) const ;
    void            cryoAttack(std::string const &target) const ;

private:
    FragTrap( void );
    std::string     _name;
    unsigned int    _hp, _max_hp, _ep, _max_ep, _lvl, _melee_dmg,
    _range_dmg, _armor_dmg_reduction, _pistol_dmg, _shotgun_dmg, _fire_dmg, _corrosive_dmg, _cryo_dmg;
};

std::ostream        &operator<<( std::ostream &out, FragTrap const &val );

#endif
