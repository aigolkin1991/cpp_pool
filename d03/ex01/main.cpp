//
// Created by Artem Iholkin on 10/3/18.
//
#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int     main( void ){
    std::string name = "First_bot_scav_trap";

    ScavTrap    bot_1(name);
    bot_1.challengeNewcomer( "Crazy scientist" );
//    std::cout << bot_1;
//    bot_1.takeDamage(20);
//    std::cout << bot_1;
//    bot_1.beRepaired(20);
//    std::cout << bot_1;
//
    FragTrap    bot_2(name);
    bot_2.vaulthunter_dot_exe("Bieber");
//    std::cout << bot_2;
//    bot_2.takeDamage(20);
//    std::cout << bot_2;
//    bot_2.beRepaired(20);
//    std::cout << bot_2;

    return 0;
}