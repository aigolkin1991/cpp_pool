//
// Created by Artem Iholkin on 10/4/18.
//
#include "ScavTrap.hpp"

typedef void (ScavTrap::*method_function)( void ) const ;

ScavTrap::ScavTrap( void ){
    return ;
}

ScavTrap::ScavTrap( std::string name ) :
        _name(name),
        _hp(100),
        _max_hp(100),
        _ep(50),
        _max_ep(50),
        _lvl(1),
        _melee_dmg(20),
        _range_dmg(15),
        _armor_dmg_reduction(3),
        _pistol_dmg(18),
        _shotgun_dmg(22),
        _fire_dmg(22),
        _corrosive_dmg(28),
        _cryo_dmg(14)
{
    std::cout << "ScavTrap [" << name << "] was created" << std::endl;
    std::cout << "Let's get this party started!" << std::endl;
    return ;
}

ScavTrap::ScavTrap( const ScavTrap& val){
    std::cout << "ScavTrap [" << val.getName() << "] copy was created" << std::endl;
    *this = val;
    return ;
}

ScavTrap::~ScavTrap( void ){
    std::cout << "ScavTrap [" << this->getName() << "] was destroyed" << std::endl;
    return ;
}

ScavTrap&   ScavTrap::operator=( const ScavTrap& val){
    if(this != &val){
        this->setName(val.getName());
        this->setMaxHp(val.getMaxHp());
        this->setMaxEp(val.getMaxEp());
        this->setHp(val.getHp());
        this->setEp(val.getEp());
        this->setLvl(val.getLvl());
        this->setMeleeDmg(val.getMeleeDmg());
        this->setRangedDmg(val.getRangedDmg());
        this->setArmorDmgReduction(val.getArmorDmgReduction());
    }
    std::cout << "Assignation operator called" << std::endl;
    return (*this);
}


unsigned int    ScavTrap::getHp( void ) const {
    return this->_hp;
}

void            ScavTrap::setHp( unsigned int val ){
    if(val <= this->getMaxHp()){
        this->_hp = val;
    }
}

unsigned int    ScavTrap::getMaxHp( void ) const {
    return this->_max_hp;
}

void            ScavTrap::setMaxHp( unsigned int val ){
    this->_max_hp = val;
}

unsigned int    ScavTrap::getEp( void ) const {
    return this->_ep;
}

void            ScavTrap::setEp(unsigned int val ){
    if(val <= this->getMaxEp()){
        this->_ep = val;
    }else{
        this->_ep = this->getMaxEp();
    }
}

unsigned int    ScavTrap::getMaxEp( void ) const {
    return this->_max_ep;
}

void            ScavTrap::setMaxEp( unsigned int val ){
    this->_max_ep = val;
}

unsigned int    ScavTrap::getLvl( void ) const {
    return this->_lvl;
}

void            ScavTrap::setLvl( unsigned int val ){
    if(val >= 1){
        this->_lvl = val;
    }
}

unsigned int    ScavTrap::getMeleeDmg( void ) const {
    return this->_melee_dmg;
}

void            ScavTrap::setMeleeDmg( unsigned int val ){
    this->_melee_dmg = val;
}

unsigned int    ScavTrap::getRangedDmg( void ) const {
    return this->_range_dmg;
}

void            ScavTrap::setRangedDmg( unsigned int val ){
    this->_range_dmg = val;
}

unsigned int    ScavTrap::getPistolDmg( void ) const {
    return this->_pistol_dmg;
}

void            ScavTrap::setPistolDmg( unsigned int val ){
    if(val > 0){
        this->_pistol_dmg = val;
    }
}

unsigned int    ScavTrap::getShotGunDmg( void ) const {
    return this->_shotgun_dmg;
}

void            ScavTrap::setShotGunDmg( unsigned int val ){
    if(val > 0){
        this->_shotgun_dmg = val;
    }
}

unsigned int    ScavTrap::getCorrosiveDmg( void ) const {
    return this->_corrosive_dmg;
}

void            ScavTrap::setCorrosiveDmg( unsigned int val ){
    if(val > 0){
        this->_corrosive_dmg = val;
    }
}

unsigned int    ScavTrap::getCryoDmg( void ) const {
    return this->_cryo_dmg;
}

void            ScavTrap::setCryoDmg( unsigned int val ){
    if(val > 0){
        this->_cryo_dmg = val;
    }
}

unsigned int    ScavTrap::getFireDmg( void ) const {
    return this->_fire_dmg;
}

void            ScavTrap::setFireDmg( unsigned int val ){
    if(val > 0){
        this->_fire_dmg = val;
    }
}

void            ScavTrap::pistolAttack(std::string const &target) const {
    std::cout << "I'd do anything for a woman with a gun." << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with pistol dealing " << this->getPistolDmg() << " points of damage]" << std::endl;
}

void            ScavTrap::shotgunAttack(std::string const &target) const {
    std::cout << "In yo' FACE!" << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with shotgun dealing " << this->getPistolDmg() << " points of damage]" << std::endl;
}

void            ScavTrap::fireAttack(std::string const &target) const {
    std::cout << "I am Fire, I am Death!" << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with firegun dealing " << this->getPistolDmg() << " points of damage]" << std::endl;
}

void            ScavTrap::corrosiveAttack(std::string const &target) const {
    std::cout << "Time to melt some faces." << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with acid dealing " << this->getPistolDmg() << " points of damage]" << std::endl;
}

void            ScavTrap::cryoAttack(std::string const &target) const {
    std::cout << "Know what killed the baddies? The Ice age." << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with icegun dealing " << this->getPistolDmg() << " points of damage]" << std::endl;
}

unsigned int    ScavTrap::getArmorDmgReduction( void ) const {
    return this->_armor_dmg_reduction;
}

void            ScavTrap::setArmorDmgReduction( unsigned int val ){
    this->_armor_dmg_reduction = val;
}

std::string     ScavTrap::getName( void ) const {
    return this->_name;
}

void            ScavTrap::setName( std::string new_name ){
    if(new_name.size() > 0)
        this->_name = new_name;
}


void            ScavTrap::rangedAttack(std::string const & target){
    std::cout << "ScavTrap " << this->getName() << " attacks " << target << " at range, causing " << this->getRangedDmg() << " points of damage !" << std::endl;
}

void            ScavTrap::meleeAttack(std::string const & target){
    std::cout << "ScavTrap " << this->getName() << " attacks " << target << " with manipulator, causing " << this->getMeleeDmg() << " points of damage !" << std::endl;
}

void            ScavTrap::takeDamage(unsigned int amount){
    unsigned int    reduced = 0;

    if(this->getHp() == 0){
        std::cout   << "ScavTrap " << this->getName() << " completely dead. Does this make you feel like man - attacking hepless heap of rusty metal" << std::endl;
        return;
    }

    if(amount > this->getArmorDmgReduction())
        reduced = (amount - this->getArmorDmgReduction());

    if( reduced >= this->getHp() ){
        std::cout   << "ScavTrap " << this->getName()
                    << " takes " << amount << " points of damage, shield reduces demage to "
                    << reduced << " , but it won't help " << this->getName() << " became destroyed" << std::endl;
        this->setHp(0);
    }else{
        std::cout << "ScavTrap " << this->getName()
                  << " takes " << amount << " points of damage, shield reduces demage to "
                  << reduced << ". Healt points left: " << (this->getHp() - reduced) << std::endl;
        this->setHp(this->getHp() - reduced);
    }
}

void            ScavTrap::beRepaired(unsigned int amount){
    if(this->getEp() == 0){
        std::cout << "ScavTrap have no energy to repair" << std::endl;
        return;
    }

    unsigned int    allowedAmount = this->getEp() >= amount ? amount : this->getEp();
    unsigned int    resultingHp = allowedAmount + this->getHp();

    if(this->getMaxHp() == this->getHp()){
        std::cout   << "ScavTrap " << this->getName() << " doesn't require repair" << std::endl;
        return;
    }else if(this->getHp() == 0){
        std::cout   << "ScavTrap " << this->getName() << " completely dead and can't be repaired" << std::endl;
        return;
    }

    if(resultingHp >= this->getMaxHp()){
        std::cout   << "ScavTrap " << this->getName() << " has been fully repaired, and now have " << this->getMaxHp() << " health points" << std::endl;
        this->setEp(this->getEp() - (this->getMaxHp() - this->getHp()));
        this->setHp(this->getMaxHp());
    }else{
        std::cout   << "ScavTrap " << this->getName() << " has restored " << allowedAmount << " health points, and now have " << resultingHp << " health points" << std::endl;
        this->setHp(resultingHp);
        this->setEp(this->getEp() - allowedAmount);
    }
}

void            ScavTrap::challenge_1( void ) const {
    std::cout << "Come back here later in a dress of your girlfriend" << std::endl;
}

void            ScavTrap::challenge_2( void ) const {
    std::cout << "Bring me some icecream made of unicorn milk" << std::endl;
}

void            ScavTrap::challenge_3( void ) const {
    std::cout << "Shoot that annoying squirrel using just old grandfather's rifle" << std::endl;
}

void            ScavTrap::challenge_4( void ) const {
    std::cout << "Here is toothpaste, squeeze it out. Ready ? Now pull it back" << std::endl;
}

void            ScavTrap::challenge_5( void ) const {
    std::cout << "Santa is not real - now live with that" << std::endl;
}

void            ScavTrap::challengeNewcomer( std::string const &target ) const {
    method_function flp[5] = {
            &ScavTrap::challenge_1,
            &ScavTrap::challenge_2,
            &ScavTrap::challenge_3,
            &ScavTrap::challenge_4,
            &ScavTrap::challenge_5
    };

    std::cout << target << ", i have a challenge for you:" << std::endl;
    int challengeNb = 0;
    srand(time( 0 ));
    challengeNb = (rand() % 5);
    method_function func = flp[challengeNb];
    (this->*func)();
}

std::ostream	&operator<<(std::ostream &out, ScavTrap const &val) {
    out << std::setw(13) << "Name: " << val.getName() << std::endl
        << std::setw(13) << "Level: " << val.getLvl() << std::endl
        << std::setw(13) << "Hp: " << val.getHp() << "/" << val.getMaxHp() << std::endl
        << std::setw(13) << "Ep: " << val.getEp() << "/" << val.getMaxEp() << std::endl
        << std::setw(13) << "Melee atck: " << val.getMeleeDmg() << std::endl
        << std::setw(13) << "Ranged atck: " << val.getRangedDmg() << std::endl
        << std::setw(13) << "Armor: " << val.getArmorDmgReduction() << std::endl;
    return (out);
}