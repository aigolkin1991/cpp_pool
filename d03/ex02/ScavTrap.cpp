//
// Created by Artem Iholkin on 10/4/18.
//
#include "ScavTrap.hpp"

ScavTrap::ScavTrap( void ){
    return ;
}

ScavTrap::ScavTrap( std::string name )  : ClapTrap(name){
    this->setMaxEp(100);
    this->setMaxHp(100);
    this->setEp(50);
    this->setHp(50);
    this->setLvl(1);
    this->setMeleeDmg(20);
    this->setRangedDmg(15);
    this->setArmorDmgReduction(3);

    std::cout << name << " ScavTrap was created" << std::endl;
    return ;
}

ScavTrap::ScavTrap( const ScavTrap& val){
    std::cout << "ScavTrap  copy was created" << std::endl;
    *this = val;
    return ;
}

ScavTrap::~ScavTrap( void ){
    std::cout << "ScavTrap was destroyed" << std::endl;
    return ;
}

ScavTrap&   ScavTrap::operator=( const ScavTrap& val){
    if(this != &val){
        this->setName(val.getName());
        this->setMaxHp(val.getMaxHp());
        this->setMaxEp(val.getMaxEp());
        this->setHp(val.getHp());
        this->setEp(val.getEp());
        this->setLvl(val.getLvl());
        this->setMeleeDmg(val.getMeleeDmg());
        this->setRangedDmg(val.getRangedDmg());
        this->setArmorDmgReduction(val.getArmorDmgReduction());
    }
    std::cout << "Assignation operator called" << std::endl;
    return (*this);
}

std::ostream	&operator<<(std::ostream &out, ScavTrap const &val) {
    out << std::setw(13) << "Name: " << val.getName() << std::endl
        << std::setw(13) << "Level: " << val.getLvl() << std::endl
        << std::setw(13) << "Hp: " << val.getHp() << "/" << val.getMaxHp() << std::endl
        << std::setw(13) << "Ep: " << val.getEp() << "/" << val.getMaxEp() << std::endl
        << std::setw(13) << "Melee atck: " << val.getMeleeDmg() << std::endl
        << std::setw(13) << "Ranged atck: " << val.getRangedDmg() << std::endl
        << std::setw(13) << "Armor: " << val.getArmorDmgReduction() << std::endl;
    return (out);
}


void            ScavTrap::challenge_1( void ) const {
    std::cout << "Come back here later in a dress of your girlfriend" << std::endl;
}

void            ScavTrap::challenge_2( void ) const {
    std::cout << "Bring me some icecream made of unicorn milk" << std::endl;
}

void            ScavTrap::challenge_3( void ) const {
    std::cout << "Shoot that annoying squirrel using just old grandfather's rifle" << std::endl;
}

void            ScavTrap::challenge_4( void ) const {
    std::cout << "Here is toothpaste, squeeze it out. Ready ? Now pull it back" << std::endl;
}

void            ScavTrap::challenge_5( void ) const {
    std::cout << "Santa is not real - now live with that" << std::endl;
}

void            ScavTrap::challengeNewcomer( std::string const &target ) const {
    sv_challenges flp[5] = {
            &ScavTrap::challenge_1,
            &ScavTrap::challenge_2,
            &ScavTrap::challenge_3,
            &ScavTrap::challenge_4,
            &ScavTrap::challenge_5
    };

    std::cout << target << ", i have a challenge for you:" << std::endl;
    int challengeNb = 0;
    srand(time( 0 ));
    challengeNb = (rand() % 5);
    sv_challenges func = flp[challengeNb];
    (this->*func)();
}