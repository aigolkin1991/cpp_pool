//
// Created by Artem Iholkin on 10/3/18.
//
#include "FragTrap.hpp"

FragTrap::FragTrap( void ){
    return ;
}

FragTrap::FragTrap( std::string name )  : ClapTrap(name),
_pistol_dmg(22),
_shotgun_dmg(28),
_fire_dmg(17),
_corrosive_dmg(33),
_cryo_dmg(10)
{
    this->setMaxEp(100);
    this->setMaxHp(100);
    this->setEp(100);
    this->setHp(100);
    this->setLvl(1);
    this->setMeleeDmg(30);
    this->setRangedDmg(20);
    this->setArmorDmgReduction(5);

    std::cout << name << " FragTrap was created" << std::endl;
    return ;
}

FragTrap::FragTrap( const FragTrap& val){
    std::cout << "FragTrap copy was created" << std::endl;
    *this = val;
    return ;
}

FragTrap::~FragTrap( void ){
    std::cout << "FragTrap was destroyed" << std::endl;
    return ;
}

FragTrap&   FragTrap::operator=( const FragTrap& val){
    if(this != &val){
        this->setName(val.getName());
        this->setMaxHp(val.getMaxHp());
        this->setMaxEp(val.getMaxEp());
        this->setHp(val.getHp());
        this->setEp(val.getEp());
        this->setLvl(val.getLvl());
        this->setMeleeDmg(val.getMeleeDmg());
        this->setRangedDmg(val.getRangedDmg());
        this->setArmorDmgReduction(val.getArmorDmgReduction());
    }
    std::cout << "Assignation operator called" << std::endl;
    return (*this);
}

std::ostream	&operator<<(std::ostream &out, FragTrap const &val) {
    out << std::setw(13) << "Name: " << val.getName() << std::endl
        << std::setw(13) << "Level: " << val.getLvl() << std::endl
        << std::setw(13) << "Hp: " << val.getHp() << "/" << val.getMaxHp() << std::endl
        << std::setw(13) << "Ep: " << val.getEp() << "/" << val.getMaxEp() << std::endl
        << std::setw(13) << "Melee atck: " << val.getMeleeDmg() << std::endl
        << std::setw(13) << "Ranged atck: " << val.getRangedDmg() << std::endl
        << std::setw(13) << "Armor: " << val.getArmorDmgReduction() << std::endl;
    return (out);
}

unsigned int    FragTrap::getPistolDmg( void ) const {
    return this->_pistol_dmg;
}

void            FragTrap::setPistolDmg( unsigned int val ){
    if(val > 0){
        this->_pistol_dmg = val;
    }
}

unsigned int    FragTrap::getShotGunDmg( void ) const {
    return this->_shotgun_dmg;
}

void            FragTrap::setShotGunDmg( unsigned int val ){
    if(val > 0){
        this->_shotgun_dmg = val;
    }
}

unsigned int    FragTrap::getCorrosiveDmg( void ) const {
    return this->_corrosive_dmg;
}

void            FragTrap::setCorrosiveDmg( unsigned int val ){
    if(val > 0){
        this->_corrosive_dmg = val;
    }
}

unsigned int    FragTrap::getCryoDmg( void ) const {
    return this->_cryo_dmg;
}

void            FragTrap::setCryoDmg( unsigned int val ){
    if(val > 0){
        this->_cryo_dmg = val;
    }
}

unsigned int    FragTrap::getFireDmg( void ) const {
    return this->_fire_dmg;
}

void            FragTrap::setFireDmg( unsigned int val ){
    if(val > 0){
        this->_fire_dmg = val;
    }
}

void            FragTrap::pistolAttack(std::string const &target) const {
    std::cout << "I'd do anything for a woman with a gun." << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with pistol dealing " << this->_pistol_dmg << " points of damage]" << std::endl;
}

void            FragTrap::shotgunAttack(std::string const &target) const {
    std::cout << "In yo' FACE!" << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with shotgun dealing " << this->_shotgun_dmg << " points of damage]" << std::endl;
}

void            FragTrap::fireAttack(std::string const &target) const {
    std::cout << "I am Fire, I am Death!" << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with firegun dealing " << this->_fire_dmg << " points of damage]" << std::endl;
}

void            FragTrap::corrosiveAttack(std::string const &target) const {
    std::cout << "Time to melt some faces." << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with acid dealing " << this->_corrosive_dmg << " points of damage]" << std::endl;
}

void            FragTrap::cryoAttack(std::string const &target) const {
    std::cout << "Know what killed the baddies? The Ice age." << std::endl;
    std::cout << "[" << this->getName() << " attacs " << target << " with icegun dealing " << this->_cryo_dmg << " points of damage]" << std::endl;
}

void            FragTrap::vaulthunter_dot_exe(std::string const & target){
    if(this->getEp() < 25){
        std::cout << "Not enough energy points" << std::endl;
    }else{

        fg_attacks flp[5] = {
                &FragTrap::pistolAttack,
                &FragTrap::shotgunAttack,
                &FragTrap::fireAttack,
                &FragTrap::corrosiveAttack,
                &FragTrap::cryoAttack
        };

        std::cout << "Look out everybody, things are about to get awesome!" << std::endl;
        int i = 5;
        int attackNb = 0;
        while (i--){
            srand(time( 0 ) + i);
            attackNb = (rand() % 5);
            fg_attacks func = flp[attackNb];
            (this->*func)(target);
        }
        this->setEp(this->getEp() - 25);
    }
}