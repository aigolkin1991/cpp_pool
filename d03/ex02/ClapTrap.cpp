//
// Created by Artem Iholkin on 10/4/18.
//

#include "ClapTrap.hpp"

ClapTrap::ClapTrap( void ){
    std::cout << "ClapTrap default constructor called" << std::endl;
}

ClapTrap::ClapTrap( std::string name ) :
_name(name)
{
    std::cout << name << " ClapTrap constructed" << std::endl;
}

ClapTrap::ClapTrap( const ClapTrap& val ){
    std::cout << _name << " Copy was created" << std::endl;
    *this = val;
    return ;
}

ClapTrap::~ClapTrap( void ){
    std::cout << "ClapTrap has been destroyed" << std::endl;
}

ClapTrap&   ClapTrap::operator=( const ClapTrap& val){
    if(this != &val){
        this->setName(val.getName());
        this->setMaxHp(val.getMaxHp());
        this->setMaxEp(val.getMaxEp());
        this->setHp(val.getHp());
        this->setEp(val.getEp());
        this->setLvl(val.getLvl());
        this->setMeleeDmg(val.getMeleeDmg());
        this->setRangedDmg(val.getRangedDmg());
        this->setArmorDmgReduction(val.getArmorDmgReduction());
    }
    std::cout << "Assignation operator called" << std::endl;
    return (*this);
}

std::ostream	&operator<<(std::ostream &out, ClapTrap const &val) {
    out << std::setw(13) << "Name: " << val.getName() << std::endl
        << std::setw(13) << "Level: " << val.getLvl() << std::endl
        << std::setw(13) << "Hp: " << val.getHp() << "/" << val.getMaxHp() << std::endl
        << std::setw(13) << "Ep: " << val.getEp() << "/" << val.getMaxEp() << std::endl
        << std::setw(13) << "Melee atck: " << val.getMeleeDmg() << std::endl
        << std::setw(13) << "Ranged atck: " << val.getRangedDmg() << std::endl
        << std::setw(13) << "Armor: " << val.getArmorDmgReduction() << std::endl;
    return (out);
}

unsigned int    ClapTrap::getHp( void ) const {
    return this->_hp;
}

void            ClapTrap::setHp( unsigned int val ){
    if(val <= this->_max_hp){
        this->_hp = val;
    }
}

unsigned int    ClapTrap::getMaxHp( void ) const {
    return this->_max_hp;
}

void            ClapTrap::setMaxHp( unsigned int val ){
    this->_max_hp = val;
}

unsigned int    ClapTrap::getEp( void ) const {
    return this->_ep;
}

void            ClapTrap::setEp(unsigned int val ){
    if(val <= this->_max_ep){
        this->_ep = val;
    }
}

unsigned int    ClapTrap::getMaxEp( void ) const {
    return this->_max_ep;
}

void            ClapTrap::setMaxEp( unsigned int val ){
    this->_max_ep = val;
}

unsigned int    ClapTrap::getLvl( void ) const {
    return this->_lvl;
}

void            ClapTrap::setLvl( unsigned int val ){
    if(val >= 1){
        this->_lvl = val;
    }
}

unsigned int    ClapTrap::getMeleeDmg( void ) const {
    return this->_melee_dmg;
}

void            ClapTrap::setMeleeDmg( unsigned int val ){
    this->_melee_dmg = val;
}

unsigned int    ClapTrap::getRangedDmg( void ) const {
    return this->_range_dmg;
}

void            ClapTrap::setRangedDmg( unsigned int val ){
    this->_range_dmg = val;
}

unsigned int    ClapTrap::getArmorDmgReduction( void ) const {
    return this->_armor_dmg_reduction;
}

void            ClapTrap::setArmorDmgReduction( unsigned int val ){
    this->_armor_dmg_reduction = val;
}

std::string     ClapTrap::getName( void ) const {
    return this->_name;
}

void            ClapTrap::setName( std::string new_name ){
    this->_name = new_name;
}

void            ClapTrap::rangedAttack(std::string const & target){
    std::cout << "Ranged on " << target << std::endl;
}

void            ClapTrap::meleeAttack(std::string const & target){
    std::cout << "Melee on " << target << std::endl;
}

void            ClapTrap::takeDamage(unsigned int amount){
    if(this->_hp == 0){
        std::cout << "Already dead" << std::endl;
    }else{
        unsigned int reducedAmount =  (amount > (this->_armor_dmg_reduction)) ? amount - this->_armor_dmg_reduction : 0;
        unsigned int reducedHealth = ((this->_hp) > reducedAmount) ? this->_hp - reducedAmount : 0;
        if(reducedHealth == 0){
            std::cout << "Get fatal damage" << std::endl;
        }else{
            std::cout << "Get " << reducedAmount << " points of damage. " << reducedHealth << " hp left." << std::endl;
        }
        this->_hp = reducedHealth;
    }
}

void            ClapTrap::beRepaired(unsigned int amount){
    if(this->_hp == this->_max_hp || amount == 0 || this->_hp == 0){
        std::cout << "Nothing to repair" << std::endl;
    }else if(this->_ep == 0){
        std::cout << "No energy to repair" << std::endl;
    }else{
        unsigned int amountNeedToRepair = this->_max_hp - this->_hp;;
        unsigned int amountCanRepair = this->_ep >= amount ? amount : this->_ep;
        unsigned int amountRepair =  amountCanRepair >= amountNeedToRepair ? amountNeedToRepair : amountCanRepair;
        std::cout << "Try to repair "
        << amount
        << "; Can repair "
        << amountCanRepair
        << "; Need to repair "
        << amountNeedToRepair
        << "; Repair "
        << amountRepair
        << std::endl;
        this->_hp = this->_hp + amountRepair;
        this->_ep = this->_ep - amountRepair;
    }
}