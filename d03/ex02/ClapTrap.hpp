//
// Created by Artem Iholkin on 10/4/18.
//

#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

#include <iostream>
#include <string>
#include <iomanip>
#include <stdlib.h>
#include <time.h>
#include <ctime>

class ClapTrap{
public:
    ClapTrap( void );
    ClapTrap( std::string name );
    ClapTrap( const ClapTrap& val );

    ~ClapTrap( void );

    ClapTrap&           operator=( const ClapTrap& val );
    std::ostream        &operator<<( std::ostream &out);

    unsigned int    getHp( void ) const ;
    void            setHp( unsigned int val );

    unsigned int    getMaxHp( void ) const ;
    void            setMaxHp( unsigned int val );

    unsigned int    getEp( void ) const ;
    void            setEp(unsigned int val );

    unsigned int    getMaxEp( void ) const ;
    void            setMaxEp( unsigned int val );

    unsigned int    getLvl( void ) const ;
    void            setLvl( unsigned int val );

    unsigned int    getMeleeDmg( void ) const ;
    void            setMeleeDmg( unsigned int val );

    unsigned int    getRangedDmg( void ) const ;
    void            setRangedDmg( unsigned int val );

    unsigned int    getArmorDmgReduction( void ) const ;
    void            setArmorDmgReduction( unsigned int val );

    std::string     getName( void ) const ;
    void            setName( std::string new_name );

    void            rangedAttack(std::string const & target);
    void            meleeAttack(std::string const & target);
    void            takeDamage(unsigned int amount);
    void            beRepaired(unsigned int amount);

private:
    std::string     _name;
    unsigned int    _hp, _max_hp, _ep, _max_ep, _lvl, _melee_dmg,
    _range_dmg, _armor_dmg_reduction;

};

std::ostream        &operator<<( std::ostream &out, ClapTrap const &val );

#endif
