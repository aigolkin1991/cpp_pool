//
// Created by Artem Iholkin on 10/3/18.
//

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

#include "ClapTrap.hpp"

class FragTrap : public ClapTrap{
public:
    FragTrap( std::string name );
    FragTrap( const FragTrap& val );
    ~FragTrap( void );

    FragTrap&       operator=( const FragTrap& val );

    unsigned int    getPistolDmg( void ) const ;
    void            setPistolDmg( unsigned int val );

    unsigned int    getShotGunDmg( void ) const ;
    void            setShotGunDmg( unsigned int val );

    unsigned int    getCorrosiveDmg( void ) const ;
    void            setCorrosiveDmg( unsigned int val );

    unsigned int    getCryoDmg( void ) const ;
    void            setCryoDmg( unsigned int val );

    unsigned int    getFireDmg( void ) const ;
    void            setFireDmg( unsigned int val );



    void            vaulthunter_dot_exe(std::string const & target);

    //Attacks
    void            pistolAttack(std::string const &target) const ;
    void            shotgunAttack(std::string const &target) const ;
    void            fireAttack(std::string const &target) const ;
    void            corrosiveAttack(std::string const &target) const ;
    void            cryoAttack(std::string const &target) const ;

private:
    FragTrap( void );
    unsigned int    _pistol_dmg, _shotgun_dmg, _fire_dmg, _corrosive_dmg, _cryo_dmg;
};

typedef void        (FragTrap::*fg_attacks)(std::string const & target) const ;
std::ostream        &operator<<( std::ostream &out, FragTrap const &val );

#endif
