//
// Created by Artem Iholkin on 10/4/18.
//

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

#include "ClapTrap.hpp"

class ScavTrap : public ClapTrap{
public:
    ScavTrap( std::string name );
    ScavTrap( const ScavTrap& val );
    ~ScavTrap( void );

    ScavTrap&       operator=( const ScavTrap& val );

    //Chalanges
    void            challenge_1( void ) const ;
    void            challenge_2( void ) const ;
    void            challenge_3( void ) const ;
    void            challenge_4( void ) const ;
    void            challenge_5( void ) const ;

    void            challengeNewcomer( std::string const &target ) const ;

private:
    ScavTrap( void );
};

typedef void        (ScavTrap::*sv_challenges)( void ) const ;
std::ostream        &operator<<( std::ostream &out, ScavTrap const &val );

#endif