//
// Created by Artem Iholkin on 10/3/18.
//
#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

int     main( void ){
//    FragTrap ft;
//    ScavTrap st;

    std::string name_s_t("Scav_trap");
    std::string name_f_t("Frag_trap");

    std::cout << "////////////////////Frag trap test/////////////////////" << std::endl;
    FragTrap f_t(name_f_t);
    FragTrap f_t_cpy_1(f_t);
    FragTrap f_t_cpy_2 = f_t_cpy_1;

    std::cout << f_t << f_t_cpy_1 << f_t_cpy_2;

    std::cout << "////////////////////Scav trap test/////////////////////" << std::endl;

    return 0;
}