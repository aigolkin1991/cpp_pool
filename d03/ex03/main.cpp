//
// Created by Artem Iholkin on 10/3/18.
//
#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>
#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"

int     main( void ){
    std::string f_t_name("FT_Johny");
    std::string s_t_name("ST_Mary");
    std::string n_t_name("NT_Hoku");
    std::string g_name("BIG G");

    ClapTrap    g(g_name);
    FragTrap    ft(f_t_name);
    ScavTrap    st(s_t_name);
    NinjaTrap   nt(n_t_name);
//    NinjaTrap   nt_cpy_1(nt);
//    NinjaTrap   nt_cpy_2 = nt_cpy_1;

//    std::cout << nt << nt_cpy_1 << nt_cpy_2;
    nt.ninjaShoebox(ft);
    nt.ninjaShoebox(st);
    nt.ninjaShoebox(nt);
    nt.ninjaShoebox(g);

    return 0;
}