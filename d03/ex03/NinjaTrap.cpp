//
// Created by Artem Iholkin on 10/5/18.
//

#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap( void ){
    return ;
}
NinjaTrap::NinjaTrap( std::string name )  : ClapTrap(name){
    this->setMaxEp(120);
    this->setMaxHp(60);
    this->setEp(120);
    this->setHp(60);
    this->setLvl(1);
    this->setMeleeDmg(60);
    this->setRangedDmg(5);
    this->setArmorDmgReduction(0);

    std::cout << name << " NinjaTrap was created" << std::endl;
    return ;
}

NinjaTrap::NinjaTrap( const NinjaTrap& val ){
    std::cout << "NinjaTrap  copy was created" << std::endl;
    *this = val;
    return ;
}

NinjaTrap::~NinjaTrap( void ){
    std::cout << "NinjaTrap was destroyed" << std::endl;
    return ;
}

NinjaTrap&          NinjaTrap::operator=( const NinjaTrap& val ){
    if(this != &val){
        this->setName(val.getName());
        this->setMaxHp(val.getMaxHp());
        this->setMaxEp(val.getMaxEp());
        this->setHp(val.getHp());
        this->setEp(val.getEp());
        this->setLvl(val.getLvl());
        this->setMeleeDmg(val.getMeleeDmg());
        this->setRangedDmg(val.getRangedDmg());
        this->setArmorDmgReduction(val.getArmorDmgReduction());
    }
    std::cout << "Assignation operator called" << std::endl;
    return (*this);
}

std::ostream        &operator<<( std::ostream &out, NinjaTrap const &val ){
    out << std::setw(13) << "Name: " << val.getName() << std::endl
        << std::setw(13) << "Level: " << val.getLvl() << std::endl
        << std::setw(13) << "Hp: " << val.getHp() << "/" << val.getMaxHp() << std::endl
        << std::setw(13) << "Ep: " << val.getEp() << "/" << val.getMaxEp() << std::endl
        << std::setw(13) << "Melee atck: " << val.getMeleeDmg() << std::endl
        << std::setw(13) << "Ranged atck: " << val.getRangedDmg() << std::endl
        << std::setw(13) << "Armor: " << val.getArmorDmgReduction() << std::endl;
    return (out);
}

void NinjaTrap::ninjaShoebox(ScavTrap const &scav) const {
    std::cout << "Soon you will see the ancestors " << scav.getName() << std::endl;
}

void NinjaTrap::ninjaShoebox(FragTrap const &frag) const {
    std::cout << "Go away trashcan " << frag.getName() << std::endl;
}

void NinjaTrap::ninjaShoebox(NinjaTrap const &ninja) const {
    std::cout << "Hello my brother " << ninja.getName() << std::endl;
}

void NinjaTrap::ninjaShoebox(ClapTrap const &clap) const {
    std::cout << "Mother of all!! " << clap.getName() << std::endl;
}