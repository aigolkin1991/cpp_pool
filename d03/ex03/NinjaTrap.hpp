//
// Created by Artem Iholkin on 10/5/18.
//

#ifndef NINJATRAP_HPP
# define NINJATRAP_HPP

#include "ScavTrap.hpp"
#include "FragTrap.hpp"
#include "ClapTrap.hpp"

class NinjaTrap : public ClapTrap{
public:
    NinjaTrap( std::string name );
    NinjaTrap( const NinjaTrap& val );
    ~NinjaTrap( void );

    NinjaTrap&          operator=( const NinjaTrap& val );

    void ninjaShoebox(ScavTrap const &scav) const ;
    void ninjaShoebox(FragTrap const &frag) const ;
    void ninjaShoebox(NinjaTrap const &ninja) const ;
    void ninjaShoebox(ClapTrap const &clap) const ;


private:
    NinjaTrap( void );
};

std::ostream        &operator<<( std::ostream &out, NinjaTrap const &val );

#endif
