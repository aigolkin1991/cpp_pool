//
// Created by Artem Iholkin on 10/11/18.
//

#ifndef D08_EASYFIND_HPP
#define D08_EASYFIND_HPP

#include <iostream>
#include <string>
#include <exception>
#include <iomanip>

template <typename T>
void    easyfind(T &container, int n){
	typename T::iterator st = container.begin();
	typename T::iterator fi = container.end();
	for(;st != fi; st++){
		if(*st == n)
			return;
	}
	throw std::exception();
}

#endif //D08_EASYFIND_HPP
