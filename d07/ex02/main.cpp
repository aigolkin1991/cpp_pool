//
// Created by Artem Iholkin on 10/11/18.
//
#include "Array.tpp"

int     main(){

	int * a = new int();
	std::cout << *a << std::endl;

	Array<char> ch_arr_1;
	Array<char> ch_arr_2(10);

	ch_arr_2[1] = 's';
	ch_arr_2[2] = 'F';
	ch_arr_2[3] = 'g';
	ch_arr_2[4] = '3';
	ch_arr_2[5] = 'g';
	ch_arr_2[6] = 'H';
	ch_arr_2[7] = 'e';

	std::cout       << ch_arr_2[1] << ", "
			<< ch_arr_2[2] << ", "
			<< ch_arr_2[3] << ", "
			<< ch_arr_2[4] << ", "
			<< ch_arr_2[5] << ", "
			<< ch_arr_2[6] << ", "
			<< std::endl;

	try {
		std::cout       << ch_arr_1[1] << ", "
		                << ch_arr_1[2] << ", "
		                << ch_arr_1[3] << ", "
		                << ch_arr_1[4] << ", "
		                << ch_arr_1[5] << ", "
		                << ch_arr_1[6] << ", "
		                << std::endl;
	}catch(std::exception &e){
		std::cout << e.what() << std::endl;
	}


	ch_arr_1 = ch_arr_2;

	try {
		std::cout       << ch_arr_1[1] << ", "
		                << ch_arr_1[2] << ", "
		                << ch_arr_1[3] << ", "
		                << ch_arr_1[4] << ", "
		                << ch_arr_1[5] << ", "
		                << ch_arr_1[6] << ", "
		                << std::endl;
	}catch(std::exception &e){
		std::cout << e.what() << std::endl;
	}

	Array<char> ch_arr_3(ch_arr_2);

	try {
		std::cout       << ch_arr_3[1] << ", "
		                << ch_arr_3[2] << ", "
		                << ch_arr_3[3] << ", "
		                << ch_arr_3[4] << ", "
		                << ch_arr_3[5] << ", "
		                << ch_arr_3[6] << ", "
		                << std::endl;
	}catch(std::exception &e){
		std::cout << e.what() << std::endl;
	}

	Array<int> ch_arr_4(10);
	try {
		std::cout       << ch_arr_4[1] << ", "
		                << ch_arr_4[2] << ", "
		                << ch_arr_4[3] << ", "
		                << ch_arr_4[4] << ", "
		                << ch_arr_4[5] << ", "
		                << ch_arr_4[6] << ", "
		                << std::endl;
	}catch(std::exception &e){
		std::cout << e.what() << std::endl;
	}

	try {
		std::cout       << ch_arr_4[11] << ", " << std::endl;
	}catch(std::exception &e){
		std::cout << e.what() << std::endl;
	}

	ch_arr_4[1] = 124;
	ch_arr_4[2] = 45;
	ch_arr_4[3] = 234;
	ch_arr_4[4] = 12;
	ch_arr_4[5] = 46346;
	ch_arr_4[6] = 234;
	ch_arr_4[7] = 2;

	try {
		std::cout       << ch_arr_4[1] << ", "
		                << ch_arr_4[2] << ", "
		                << ch_arr_4[3] << ", "
		                << ch_arr_4[4] << ", "
		                << ch_arr_4[5] << ", "
		                << ch_arr_4[6] << ", "
		                << std::endl;
	}catch(std::exception &e){
		std::cout << e.what() << std::endl;
	}

	return 0;
}