//
// Created by Artem Iholkin on 10/11/18.
//

#include "Array.tpp"

template <typename T>
Array<T>::OutOfRangeException::OutOfRangeException(void) { return ; }
template <typename T>
Array<T>::OutOfRangeException::OutOfRangeException(const OutOfRangeException& src) { *this = src; }
template <typename T>
Array<T>::OutOfRangeException::~OutOfRangeException(void) throw() { return ; }
template <typename T>
const char*Array<T>::OutOfRangeException::what(void) const throw() { return ("Index out of range"); }

template <typename T>
Array<T>::Array() {
	this->_array_ptr = new T();
	this->_size = 0;
	return ;
}

template <typename T>
Array<T>::Array(unsigned int n) {
	this->_array_ptr = new T [n];
	this->_size = n;
}

template <typename T>
Array<T>::Array(Array const &src) {
	*this = src;
	return ;
}

template <typename T>
Array<T>::~Array() {
	delete this->_array_ptr;
	return;
}

template <typename T>
unsigned int    Array<T>::getSize() const { return this->_size;}

template <typename T>
T*              Array<T>::getArrayPtr() const { return this->_array_ptr;}

template <typename T>
Array<T>	&Array<T>::operator=(Array<T> const &src) {
	delete (this->_array_ptr);
	this->_size = src.getSize();
	this->_array_ptr = new T[this->_size];

	T*      srcData = src.getArrayPtr();
	for (unsigned int i = 0; i < this->_size; i++)
		this->_array_ptr[i] = srcData[i];
	return (*this);
}

template <typename T>
T               &Array<T>::operator[](unsigned int i) throw(OutOfRangeException) {
	if(!this->_array_ptr || this->_size >= i){
		throw OutOfRangeException();
	}else{
		return this->_array_ptr[i];
	}
}