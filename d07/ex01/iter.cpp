//
// Created by Artem Iholkin on 10/10/18.
//

#include <iostream>
#include <string>

template<typename T>
void      iter(T *addr, size_t len, void (*func)(T &)){
	for(size_t i = 0; i < len; i++){
		(*func)(addr[i]);
	}
}

template<typename T>
void    print(T &var ){
	std::cout << var << std::endl;
}

template<typename T>
void    printSumm(T &var ){
	static T summ = 0;
	std::cout << (summ += var) << std::endl;
}



int     main(void){
	int     arr_1[5] = {1,2,3,4,5};
	iter<int>(arr_1, 5, &printSumm<int>);

	char    arr_2[5] = {'a', 'b', 'c', 'd', 'e'};
	iter<char>(arr_2, 5, &print<char>);
}