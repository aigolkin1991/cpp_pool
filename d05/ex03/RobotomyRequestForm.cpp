//
// Created by Artem Iholkin on 10/8/18.
//
#include "RobotomyRequestForm.hpp"

RobotomyRequestForm::RobotomyRequestForm(std::string target) : Form("Robotomy", 72, 45) {
    this->_target = target;
    return ;
}
RobotomyRequestForm::RobotomyRequestForm(const RobotomyRequestForm &src) : Form("Robotomy", 72, 45)  {
    *this = src;
    return ;
}
RobotomyRequestForm::~RobotomyRequestForm(void) {
    return ;
}




std::string                      RobotomyRequestForm::getTarget(void) const {
    return this->_target;
}
void                            RobotomyRequestForm::action(std::string target) const {
    std::cout << "Action on " << target << std::endl;
    srand(time(0));
    if (rand() & 1)
    {
        std::cout << "BZZZZZZZZZZ..." << target << " has been robotomized successfully !" << std::endl;
    }
    else
    {
        std::cout << "The " << target << "robotomization failed !" << std::endl;
    }
    return ;

}



RobotomyRequestForm&          RobotomyRequestForm::operator=(const RobotomyRequestForm &rhs)
{
    if (this != &rhs)
    {
        this->_target = rhs.getTarget();
    }
    return (*this);
}
std::ostream&                   operator<<( std::ostream &out, RobotomyRequestForm const &src ){
    out << ((Form&)src) << std::endl;
    out << "form target: [" << src.getTarget() << "]";
    return out;
}
