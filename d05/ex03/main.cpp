//
// Created by Artem Iholkin on 10/9/18.
//

#include <iostream>
#include <string>
#include "Form.hpp"
#include "ShrubberyCreationForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "Intern.hpp"

int     main(void){
    Intern  newIntern;
    Form*   newForm;

    try {
        newForm = newIntern.makeForm("robotomy_request", "Paperbox");
        std::cout << *newForm << std::endl << "ADDR: " << newForm << std::endl;
        delete newForm;
        newForm = newIntern.makeForm("presidential_pardon", "Sandbox");
        std::cout << *newForm << std::endl << "ADDR: " << newForm << std::endl;
        delete newForm;
        newForm = newIntern.makeForm("shrubbery_creation", "Shoebox");
        std::cout << *newForm << std::endl << "ADDR: " << newForm << std::endl;
        delete newForm;
        newForm = newIntern.makeForm("random form", "Shoebox");
        std::cout << "ADDR: " << newForm << std::endl;
    }catch(std::exception& e) {
        std::cout << "Form can't be created, reason: " << std::endl << e.what() << std::endl;
    }

    return 0;
}