//
// Created by Artem Iholkin on 10/9/18.
//

#ifndef D05_INTERN_HPP
#define D05_INTERN_HPP

#include <iostream>
#include "Form.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

class Intern{
    class FormNotFoundException : public std::exception
    {
    public:
        FormNotFoundException(void);
        FormNotFoundException(const FormNotFoundException &src);

        virtual ~FormNotFoundException(void) throw();

        FormNotFoundException &operator= (const FormNotFoundException &rhs);

        virtual const char    *what(void) const throw();
    };
public:
    Intern(void);
    Intern(const Intern &src);
    ~Intern(void);

    Form*       makeForm(std::string form_name, std::string form_target);

    Intern&     operator=(const Intern &src);
};

#endif //D05_INTERN_HPP
