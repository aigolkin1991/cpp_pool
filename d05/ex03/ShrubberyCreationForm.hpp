//
// Created by Artem Iholkin on 10/8/18.
//

#ifndef D05_SHRUBBERYCREATIONFORM_HPP
#define D05_SHRUBBERYCREATIONFORM_HPP

#include "Form.hpp"


class ShrubberyCreationForm : public Form{
public:
    ShrubberyCreationForm(std::string target);
    ShrubberyCreationForm(const ShrubberyCreationForm &src);
    ~ShrubberyCreationForm(void);

    std::string                         getTarget(void) const ;
    void                                action(std::string target) const ;

    ShrubberyCreationForm&              operator=(const ShrubberyCreationForm &src);

private:
    ShrubberyCreationForm(void);

    std::string                         _target;
};

std::ostream&       operator<<( std::ostream &out, ShrubberyCreationForm const &src );

#endif //D05_SHRUBBERYCREATIONFORM_HPP
