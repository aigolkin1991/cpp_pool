//
// Created by Artem Iholkin on 10/8/18.
//

#include "ShrubberyCreationForm.hpp"

ShrubberyCreationForm::ShrubberyCreationForm(std::string target) : Form("Shrubbery", 145, 137) {
    this->_target = target;
    return ;
}
ShrubberyCreationForm::ShrubberyCreationForm(const ShrubberyCreationForm &src) : Form("Shrubbery", 145, 137)  {
    *this = src;
    return ;
}
ShrubberyCreationForm::~ShrubberyCreationForm(void) {
    return ;
}




std::string                      ShrubberyCreationForm::getTarget(void) const {
    return this->_target;
}
void                            ShrubberyCreationForm::action(std::string target) const {
    std::cout << "Action on " << target << std::endl;

    std::string     file_name(target+"_shrubbery");
    std::string     tree;
    std::ofstream   file;

    tree = "    oxoxoo    ooxoo\n"
           "  ooxoxo oo  oxoxooo\n"
           " oooo xxoxoo ooo ooox\n"
           " oxo o oxoxo  xoxxoxo\n"
           "  oxo xooxoooo o ooo\n"
           "    ooo\\oo\\  /o/o\n"
           "        \\  \\/ /\n"
           "         |   /\n"
           "         |  |\n"
           "         | D|\n"
           "         |  |\n"
           "         |  |\n"
           "  ______/____\\____\n";

    file.open(file_name, std::ios::in | std::ios::app);
    if(file.is_open())
        file << tree << std::endl;
    file.close();
    return;
}



ShrubberyCreationForm&          ShrubberyCreationForm::operator=(const ShrubberyCreationForm &rhs)
{
    if (this != &rhs)
    {
        this->_target = rhs.getTarget();
    }
    return (*this);
}
std::ostream&                   operator<<( std::ostream &out, ShrubberyCreationForm const &src ){
    out << ((Form&)src) << std::endl;
    out << "form target: [" << src.getTarget() << "]";
    return out;
}