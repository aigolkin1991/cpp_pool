//
// Created by Artem Iholkin on 10/9/18.
//
#include "Intern.hpp"

Intern::Intern(void) {
    return ;
}

Intern::Intern(const Intern &src) {
    *this = src;
    return ;
}

Intern::~Intern(void) {
    return;
}

Form*           Intern::makeForm(std::string form_name, std::string form_target) {
    Form    *new_form = 0;

    if(form_name == "robotomy_request"){
        new_form = new RobotomyRequestForm(form_target);
    }else if(form_name == "presidential_pardon"){
        new_form = new PresidentialPardonForm(form_target);
    }else if(form_name == "shrubbery_creation"){
        new_form = new ShrubberyCreationForm(form_target);
    }else{
        std::cout << "No such form [" << form_name << "]" << std::endl;
        throw FormNotFoundException();
    }

    return new_form;
}

Intern&         Intern::operator=(const Intern &src){
    if(this != &src)
        return (*this);
    return (*this);
}

Intern::FormNotFoundException::FormNotFoundException(void)
{
    return ;
}

Intern::FormNotFoundException::FormNotFoundException(const FormNotFoundException &src)
{
    *this = src;
    return ;
}

Intern::FormNotFoundException::~FormNotFoundException(void) throw()
{
    return ;
}

Intern::FormNotFoundException &Intern::FormNotFoundException::operator= (const FormNotFoundException &rhs)
{
    static_cast <void> (rhs);
    return (*this);
}

const char *Intern::FormNotFoundException::what(void) const throw()
{
    return ("No such form...");
}
