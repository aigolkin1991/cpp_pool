//
// Created by Artem Iholkin on 10/8/18.
//

#ifndef D05_BUREAUCRAT_HPP
#define D05_BUREAUCRAT_HPP

#include <iostream>
#include <string>
#include <stdexcept>
#include "Form.hpp"

class Form;

class Bureaucrat{
public:

    class LowGradeException : public std::exception
    {
    public:
        LowGradeException(void);
        LowGradeException(const LowGradeException &src);

        virtual ~LowGradeException(void) throw();

        LowGradeException  &operator= (const LowGradeException &rhs);

        virtual const char    *what(void) const throw();
    };

    class HighGradeException : public std::exception
    {
    public:
        HighGradeException(void);
        HighGradeException(const HighGradeException &src);

        virtual ~HighGradeException(void) throw();

        HighGradeException &operator= (const HighGradeException &rhs);

        virtual const char    *what(void) const throw();
    };

    Bureaucrat(std::string name, unsigned int grade);
    Bureaucrat(const Bureaucrat &src);
    ~Bureaucrat(void);

    Bureaucrat&         operator=(const Bureaucrat &src);

    void                incrementGrade(void);
    void                decrementGrade(void);
    std::string         getName(void) const;
    unsigned int        getGrade(void) const ;
    void                signForm(Form &f) const ;

private:
    Bureaucrat(void);

    const std::string   _name;
    unsigned int        _grade;
};

std::ostream&       operator<<( std::ostream &out, Bureaucrat const &src );

#endif //D05_BUREAUCRAT_HPP