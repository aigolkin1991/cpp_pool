//
// Created by Artem Iholkin on 10/8/18.
//
#include <iostream>
#include <stdexcept>
#include "Bureaucrat.hpp"
#include "Form.hpp"


int     main(void){
    std::string     name("Form_a");

    std::cout << "////////////START CANONICAL TEST///////////" << std::endl;
    Form            a(name, 10, 12);
    Form            b = a;
    Form            c(b);

    std::cout << a << std::endl;
    std::cout << b << std::endl;
    std::cout << c << std::endl;
    std::cout << "////////////END CANONICAL TEST///////////" << std::endl;

    std::cout << "////////////START EXCEPTION TEST///////////" << std::endl;
    std::string     name_1("Vasiliy_l");
    std::string     name_2("Evpatiy_h");
    Bureaucrat      d(name_1, 15);
    Bureaucrat      e(name_2, 9);
    Form            f(name, 10, 12);

    std::cout << f << std::endl;
    std::cout << "Bureaucrat: " << std::endl << d << std::endl;
    d.signForm(f);

    std::cout << f << std::endl;
    std::cout << "Bureaucrat: " << std::endl << e << std::endl;
    e.signForm(f);


    std::cout << "////////////END EXCEPTION TEST///////////" << std::endl;

    return 0;
}