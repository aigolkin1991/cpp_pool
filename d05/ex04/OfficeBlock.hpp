//
// Created by Artem Iholkin on 10/9/18.
//

#ifndef D05_OFFICEBLOCK_HPP
#define D05_OFFICEBLOCK_HPP

#include <iostream>
#include <string>
#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "Intern.hpp"

class OfficeBlock{
    class NoInternException : public std::exception
    {
    public:
        NoInternException(void);
        NoInternException(const NoInternException &src);

        virtual ~NoInternException(void) throw();

        NoInternException &operator= (const NoInternException &rhs);

        virtual const char    *what(void) const throw();
    };

    class NoSigningBureaucratException : public std::exception
    {
    public:
        NoSigningBureaucratException(void);
        NoSigningBureaucratException(const NoSigningBureaucratException &src);

        virtual ~NoSigningBureaucratException(void) throw();

        NoSigningBureaucratException &operator= (const NoSigningBureaucratException &rhs);

        virtual const char    *what(void) const throw();
    };

    class NoExecBureaucratException : public std::exception
    {
    public:
        NoExecBureaucratException(void);
        NoExecBureaucratException(const NoExecBureaucratException &src);

        virtual ~NoExecBureaucratException(void) throw();

        NoExecBureaucratException &operator= (const NoExecBureaucratException &rhs);

        virtual const char    *what(void) const throw();
    };

public:
    OfficeBlock(void);
    OfficeBlock(Intern *intern, Bureaucrat *signingBureaucrat, Bureaucrat *executingBureaucrat);
    ~OfficeBlock(void);

    void            setIntern(Intern* intern);
    void            setSigningB(Bureaucrat* b);
    void            setExecutingB(Bureaucrat* b);

    Bureaucrat*     getSigningB(void) const ;
    Bureaucrat*     getExecB(void) const ;

    void            doBureaucracy(std::string form_name, std::string target_name);

private:
    Intern*          _intern;
    Bureaucrat*      _signing_b;
    Bureaucrat*      _exec_b;

    OfficeBlock(OfficeBlock const &src);

    OfficeBlock    &operator= (const OfficeBlock &rhs);
};

std::ostream&       operator<<( std::ostream &out, OfficeBlock const &src );

#endif //D05_OFFICEBLOCK_HPP
