//
// Created by Artem Iholkin on 10/8/18.
//
#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat(void) {
    return ;
}

Bureaucrat::Bureaucrat(const Bureaucrat &src) : _name(src.getName()) {
    *this = src;
    return ;
}

Bureaucrat::Bureaucrat(std::string name, unsigned int grade) : _name(name) {
    if(grade < 1){
        throw HighGradeException();
    }else if(grade > 150){
        throw LowGradeException();
    }else{
        this->_grade = grade;
    }
}

Bureaucrat::~Bureaucrat() {
    return;
}

std::string         Bureaucrat::getName(void) const {
    return this->_name;
}

unsigned int        Bureaucrat::getGrade(void) const {
    return this->_grade;
}

void Bureaucrat::incrementGrade(void){
    if(this->_grade <= 1)
        throw HighGradeException();
    else
        this->_grade--;
}

void Bureaucrat::decrementGrade(void){
    if(this->_grade >= 150)
        throw LowGradeException();
    else
        this->_grade++;
}

void                Bureaucrat::signForm(Form &f) const {
    try {
        f.beSigned(this);
        std::cout << "Form [" << f.getName() << "] was signed by " << this->_name << std::endl;
    }catch (std::exception& e){
        throw e.what();
//        std::cout << "Form [" << f.getName() << "] wasn't signed by " << this->_name << ": " << e.what() << std::endl;
    }
}

void                Bureaucrat::executeForm(Form const & form){
    try{
        form.execute(*this);
        std::cout << "Form [" << form.getName() << "] was executed by [" << this->_name << "]" << std::endl;
    }catch(std::exception& e) {
        throw e.what();
//        std::cout << "Form [" << form.getName() << "] can't be executed by [" << this->_name << "], reason: " << e.what() << std::endl;
    }
}



Bureaucrat&         Bureaucrat::operator=(const Bureaucrat &src){
    this->_grade = src.getGrade();
    return *this;
}

std::ostream&       operator<<( std::ostream &out, Bureaucrat const &src ){
    out << "Name: " << src.getName() << ", grade: " << src.getGrade();
    return out;
}



Bureaucrat::LowGradeException::LowGradeException(void)
{
    return ;
}

Bureaucrat::LowGradeException::LowGradeException(const LowGradeException &src)
{
    *this = src;
    return ;
}

Bureaucrat::LowGradeException::~LowGradeException(void) throw()
{
    return ;
}

Bureaucrat::LowGradeException &Bureaucrat::LowGradeException::operator= (const LowGradeException &rhs)
{
    (void)rhs;
    return (*this);
}

const char
*Bureaucrat::LowGradeException::what(void) const throw()
{
    return ("Grade is too low...");
}

Bureaucrat::HighGradeException::HighGradeException(void)
{
    return ;
}

Bureaucrat::HighGradeException::HighGradeException(const HighGradeException &src)
{
    *this = src;
    return ;
}

Bureaucrat::HighGradeException::~HighGradeException(void) throw()
{
    return ;
}

Bureaucrat::HighGradeException &Bureaucrat::HighGradeException::operator= (const HighGradeException &rhs)
{
    (void)rhs;
    return (*this);
}

const char *Bureaucrat::HighGradeException::what(void) const throw()
{
    return ("Grade is too high...");
}