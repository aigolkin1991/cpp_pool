//
// Created by Artem Iholkin on 10/8/18.
//

#ifndef D05_ROBOTOMYREQUESTFORM_HPP
#define D05_ROBOTOMYREQUESTFORM_HPP

#include "Form.hpp"

class RobotomyRequestForm : public Form{
public:
    RobotomyRequestForm(std::string target);
    RobotomyRequestForm(const RobotomyRequestForm &src);
    ~RobotomyRequestForm(void);

    std::string                         getTarget(void) const ;
    void                                action(std::string target) const ;

    RobotomyRequestForm&              operator=(const RobotomyRequestForm &src);

private:
    RobotomyRequestForm(void);

    std::string                         _target;
};

std::ostream&       operator<<( std::ostream &out, RobotomyRequestForm const &src );


#endif //D05_ROBOTOMYREQUESTFORM_HPP
