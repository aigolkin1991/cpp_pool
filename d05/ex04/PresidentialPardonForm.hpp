//
// Created by Artem Iholkin on 10/9/18.
//

#ifndef D05_PRESIDENTIALPARDONFORM_HPP
#define D05_PRESIDENTIALPARDONFORM_HPP

#include "Form.hpp"

class PresidentialPardonForm : public Form{
public:
    PresidentialPardonForm(std::string target);
    PresidentialPardonForm(const PresidentialPardonForm &src);
    ~PresidentialPardonForm(void);

    std::string                         getTarget(void) const ;
    void                                action(std::string target) const ;

    PresidentialPardonForm&              operator=(const PresidentialPardonForm &src);

private:
    PresidentialPardonForm(void);

    std::string                         _target;
};

std::ostream&       operator<<( std::ostream &out, PresidentialPardonForm const &src );

#endif //D05_PRESIDENTIALPARDONFORM_HPP
