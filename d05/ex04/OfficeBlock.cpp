//
// Created by Artem Iholkin on 10/9/18.
//
#include "OfficeBlock.hpp"

OfficeBlock::OfficeBlock(void) : _intern(NULL), _signing_b(NULL), _exec_b(NULL) {
    return ;
}

OfficeBlock::OfficeBlock(Intern *intern, Bureaucrat *signingBureaucrat, Bureaucrat *executingBureaucrat) :
_intern(intern),
_signing_b(signingBureaucrat),
_exec_b(executingBureaucrat)
{
    return ;
}

OfficeBlock::~OfficeBlock(void) {
    return ;
}

void    OfficeBlock::setIntern(Intern* intern){
    this->_intern = intern;
}
void    OfficeBlock::setSigningB(Bureaucrat* b){
    this->_signing_b = b;
}
void    OfficeBlock::setExecutingB(Bureaucrat* b){
    this->_exec_b = b;
}

Bureaucrat*     OfficeBlock::getSigningB(void) const {
    return this->_signing_b;
}

Bureaucrat*     OfficeBlock::getExecB(void) const {
    return this->_exec_b;
}

void            OfficeBlock::doBureaucracy(std::string form_name, std::string target_name){
    if(this->_intern == NULL)
        throw NoInternException();
    else if(this->_signing_b == NULL)
        throw NoSigningBureaucratException();
    else if(this->_exec_b == NULL)
        throw NoExecBureaucratException();
    else{
        Form    *newForm = NULL;

        try {
            newForm = this->_intern->makeForm(form_name, target_name);
            std::cout << "new form created: " << std::endl << *newForm << std::endl;
        }catch(std::exception &e){
            throw e.what();
        }

        try {
            this->_signing_b->signForm(*newForm);
            std::cout << "form signed by bureaucrat " << this->_signing_b->getName() << std::endl;
        }catch (char const* e){
            throw e;
        }

        try {
            this->_exec_b->executeForm(*newForm);
            std::cout << "form executed by bureaucrat " << this->_exec_b->getName() << std::endl;
        }catch(char const* e){
            throw e;
        }
    }
}

std::ostream&       operator<<( std::ostream &out, OfficeBlock const &src ){
    if(src.getSigningB() != NULL)
        out << (*src.getSigningB()) << std::endl;
    if(src.getExecB() != NULL)
        out << (*src.getExecB());
    return out;
}



//EXCEPTIONS


OfficeBlock::NoInternException::NoInternException(void)
{
    return ;
}

OfficeBlock::NoInternException::NoInternException(const NoInternException& src)
{
    *this = src;
    return ;
}

OfficeBlock::NoInternException::~NoInternException(void) throw()
{
    return ;
}

OfficeBlock::NoInternException &OfficeBlock::NoInternException::operator= (const NoInternException &rhs)
{
    static_cast <void> (rhs);
    return (*this);
}

const char*OfficeBlock::NoInternException::what(void) const throw()
{
    return ("Intern not set");
}


OfficeBlock::NoSigningBureaucratException::NoSigningBureaucratException(void)
{
    return ;
}

OfficeBlock::NoSigningBureaucratException::NoSigningBureaucratException(const NoSigningBureaucratException& src)
{
    *this = src;
    return ;
}

OfficeBlock::NoSigningBureaucratException::~NoSigningBureaucratException(void) throw()
{
    return ;
}

OfficeBlock::NoSigningBureaucratException &OfficeBlock::NoSigningBureaucratException::operator= (const NoSigningBureaucratException &rhs)
{
    static_cast <void> (rhs);
    return (*this);
}

const char*OfficeBlock::NoSigningBureaucratException::what(void) const throw()
{
    return ("Signing bureaucrat not set");
}

OfficeBlock::NoExecBureaucratException::NoExecBureaucratException(void)
{
    return ;
}

OfficeBlock::NoExecBureaucratException::NoExecBureaucratException(const NoExecBureaucratException& src)
{
    *this = src;
    return ;
}

OfficeBlock::NoExecBureaucratException::~NoExecBureaucratException(void) throw()
{
    return ;
}

OfficeBlock::NoExecBureaucratException &OfficeBlock::NoExecBureaucratException::operator= (const NoExecBureaucratException &rhs)
{
    static_cast <void> (rhs);
    return (*this);
}

const char*OfficeBlock::NoExecBureaucratException::what(void) const throw()
{
    return ("Executing bureaucrat not set");
}