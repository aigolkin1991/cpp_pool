//
// Created by Artem Iholkin on 10/9/18.
//

#include <iostream>
#include <string>
#include "OfficeBlock.hpp"
#include "Form.hpp"
#include "ShrubberyCreationForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "Intern.hpp"

int     main(void){
    Intern          newIntern;
    std::string     b_s_name("Signing_bureaucrat");
    std::string     b_e_name("Executing_bureaucrat");
    Bureaucrat      b_s(b_s_name, 24);
    Bureaucrat      b_e(b_e_name, 6);

    OfficeBlock     ob_1(&newIntern, &b_s, &b_e);
    OfficeBlock     ob_2;

    ob_2.setIntern(&newIntern);
    ob_2.setSigningB(&b_s);
    ob_2.setExecutingB(&b_e);

    std::cout << "Office bloc (1): " << std::endl << ob_1 << std::endl << "Office block (2)" << std::endl << ob_2 << std::endl;

    std::cout << "/////////////     Try ob_1    /////////////" << std::endl;
    try {
        ob_1.doBureaucracy("presidential_pardon", "Salmon");
    }catch (std::exception &e){
        std::cout << "Can't do bureaucracy. Reason: " << std::endl << e.what() << std::endl ;
    }catch (char const* e){
        std::cout << "Can't do bureaucracy. Reason: " << std::endl << e << std::endl ;
    }

    std::cout << "/////////////     Try ob_2    /////////////" << std::endl;
    try {
        ob_1.doBureaucracy("mini form", "Kraken");
    }catch (std::exception &e){
        std::cout << "Can't do bureaucracy. Reason: " << std::endl << e.what() << std::endl ;
    }catch (char const* e){
        std::cout << e << std::endl;
    }


    return 0;
}