//
// Created by Artem Iholkin on 10/8/18.
//
#include <iostream>
#include "Bureaucrat.hpp"

int main(void){
    Bureaucrat  b("Vasiliy", 2);
    Bureaucrat  c = b;
    Bureaucrat  d(c);

    std::cout << "Ori: " << b << std::endl;
    std::cout << "Asi: " << c << std::endl;
    std::cout << "Src: " << d << std::endl;

    try{
        Bureaucrat  e("Vasiliy", -1);
    }catch(std::exception& e) {
        std::cout << e.what() << std::endl;
    }

    try {
        Bureaucrat  f("Vasiliy", 151);
    }catch(std::exception& e) {
        std::cout << e.what() << std::endl;
    }
//
    int i = 10;
    while (i--){
        std::cout << b << std::endl;
        try {
            b.incrementGrade();
        }catch(std::exception& e) {
            std::cout << e.what() << std::endl;
        }
    }
//
    int j = 0;
    while (++j < 160){
        std::cout << b << std::endl;
        try {
            b.decrementGrade();
        }catch(std::exception& e) {
            std::cout << e.what() << std::endl;
        }
    }


    return 0;
}
