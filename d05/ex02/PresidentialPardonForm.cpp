//
// Created by Artem Iholkin on 10/9/18.
//

#include "PresidentialPardonForm.hpp"

PresidentialPardonForm::PresidentialPardonForm(std::string target) : Form("PresidentialPardon", 25, 5) {
    this->_target = target;
    return ;
}
PresidentialPardonForm::PresidentialPardonForm(const PresidentialPardonForm &src) : Form("PresidentialPardon", 25, 5)  {
    *this = src;
    return ;
}
PresidentialPardonForm::~PresidentialPardonForm(void) {
    return ;
}




std::string                      PresidentialPardonForm::getTarget(void) const {
    return this->_target;
}
void                            PresidentialPardonForm::action(std::string target) const {
    std::cout << "Action on " << target << std::endl;
    std::cout << target << " has been pardoned by Zaphod Beeblerbox" << std::endl;
}



PresidentialPardonForm&          PresidentialPardonForm::operator=(const PresidentialPardonForm &rhs)
{
    if (this != &rhs)
    {
        this->_target = rhs.getTarget();
    }
    return (*this);
}
std::ostream&                   operator<<( std::ostream &out, PresidentialPardonForm const &src ){
    out << ((Form&)src) << std::endl;
    out << "form target: [" << src.getTarget() << "]";
    return out;
}
