//
// Created by Artem Iholkin on 10/8/18.
//
#include <iostream>
#include <string>
#include <stdexcept>
#include "Form.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"

int     main(void){

    std::cout << "///////////   START Test shrubbery form    ///////////" << std::endl;
    std::string             target("Home");
    std::string             b_1_name("Vasiliy");
    std::string             b_2_name("Evheniy");
    std::string             b_3_name("Stepan");
    ShrubberyCreationForm   s_f(target);
    Bureaucrat              b_1(b_1_name, 148);
    Bureaucrat              b_2(b_2_name, 138);
    Bureaucrat              b_3(b_3_name, 137);
    std::cout << "**********FORM***********" << std::endl;
    std::cout << "Form: " << std::endl << s_f << std::endl;
    std::cout << "**********B_1***********" << std::endl;
    std::cout << "Bureaucrat_1: " << std::endl << b_1 << std::endl;
    std::cout << "**********B_2***********" << std::endl;
    std::cout << "Bureaucrat_2: " << std::endl << b_2 << std::endl;
    std::cout << "**********B_3***********" << std::endl;
    std::cout << "Bureaucrat_3: " << std::endl << b_3 << std::endl;
    std::cout << "**********MSG***********" << std::endl;
    b_1.executeForm(s_f);
    b_1.signForm(s_f);
    b_2.signForm(s_f);
    b_2.executeForm(s_f);
    b_3.executeForm(s_f);
    std::cout << "///////////   END Test shrubbery form    ///////////" << std::endl;

    std::cout << "///////////   START Test roboto form    ///////////" << std::endl;
    std::string             roboto_target("Paper box");
    std::string             ba_1_name("Miki");
    std::string             ba_2_name("Mini");
    std::string             ba_3_name("Tom");
    RobotomyRequestForm     r_f(roboto_target);
    Bureaucrat              ba_1(ba_1_name, 73);
    Bureaucrat              ba_2(ba_2_name, 46);
    Bureaucrat              ba_3(ba_3_name, 45);
    std::cout << "**********FORM***********" << std::endl;
    std::cout << "Form: " << std::endl << r_f << std::endl;
    std::cout << "**********B_1***********" << std::endl;
    std::cout << "Bureaucrat_1: " << std::endl << ba_1 << std::endl;
    std::cout << "**********B_2***********" << std::endl;
    std::cout << "Bureaucrat_2: " << std::endl << ba_2 << std::endl;
    std::cout << "**********B_3***********" << std::endl;
    std::cout << "Bureaucrat_3: " << std::endl << ba_3 << std::endl;
    std::cout << "**********MSG***********" << std::endl;
    ba_1.executeForm(r_f);
    ba_1.signForm(r_f);
    ba_2.signForm(r_f);
    ba_2.executeForm(r_f);
    ba_3.executeForm(r_f);
    std::cout << "///////////   END Test roboto form    ///////////" << std::endl;

    std::cout << "///////////   START Test presidential form    ///////////" << std::endl;
    std::string             president_target("Stone wall");
    std::string             baa_1_name("Nif-nif");
    std::string             baa_2_name("Naf-naf");
    std::string             baa_3_name("Nuf-nuf");
    PresidentialPardonForm  p_f(president_target);
    Bureaucrat              baa_1(baa_1_name, 73);
    Bureaucrat              baa_2(baa_2_name, 6);
    Bureaucrat              baa_3(baa_3_name, 5);
    std::cout << "**********FORM***********" << std::endl;
    std::cout << "Form: " << std::endl << p_f << std::endl;
    std::cout << "**********B_1***********" << std::endl;
    std::cout << "Bureaucrat_1: " << std::endl << baa_1 << std::endl;
    std::cout << "**********B_2***********" << std::endl;
    std::cout << "Bureaucrat_2: " << std::endl << baa_2 << std::endl;
    std::cout << "**********B_3***********" << std::endl;
    std::cout << "Bureaucrat_3: " << std::endl << baa_3 << std::endl;
    std::cout << "**********MSG***********" << std::endl;
    baa_1.executeForm(p_f);
    baa_1.signForm(p_f);
    baa_2.signForm(p_f);
    baa_2.executeForm(p_f);
    baa_3.executeForm(p_f);
    std::cout << "///////////   END Test presidential form    ///////////" << std::endl;


    return 0;
}
