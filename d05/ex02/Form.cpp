//
// Created by Artem Iholkin on 10/8/18.
//
#include "Form.hpp"

//Form::Form(void) {
//    return ;
//}

Form::Form(const Form &src) :
_name(src.getName()),
_is_signed(src.getIsSigned()),
_req_grade_to_sign(src.getReqGradeToSign()),
_req_grade_to_exec(src.getReqGradeToExec())
{
    *this = src;
    return ;
}

Form::Form(std::string name, unsigned int req_grade_to_sign, unsigned int req_grade_to_exec) :
_name(name),
_is_signed(false),
_req_grade_to_sign(req_grade_to_sign),
_req_grade_to_exec(req_grade_to_exec)
{
    return ;
}

Form::~Form(void) {
    return;
}

std::string     Form::getName(void) const {
    return this->_name;
}

unsigned int    Form::getReqGradeToSign(void) const {
    return this->_req_grade_to_sign;
}

unsigned int    Form::getReqGradeToExec(void) const {
    return this->_req_grade_to_exec;
}

bool            Form::getIsSigned(void) const {
    return this->_is_signed;
}

void            Form::beSigned(const Bureaucrat *b){
    unsigned int   b_grade = b->getGrade();

    if(this->_req_grade_to_sign > b_grade ){
        this->_is_signed = true;
    }else{
        throw LowGradeException();
    }
}

void            Form::execute(Bureaucrat const & executor) const{
    unsigned int    b_grade = executor.getGrade();
    if(!this->_is_signed){
        throw NotSignedException();
    }else if(b_grade > this->_req_grade_to_exec){
        throw LowGradeException();
    }else{
        this->action(this->getTarget());
    }

    return;
}

Form&           Form::operator= (const Form &rhs)
{
    if (this != &rhs)
    {
        this->_is_signed = rhs.getIsSigned();
    }
    return (*this);
}

std::ostream&       operator<<( std::ostream &out, Form const &src ){
    out << "Form [" << src.getName() << "] currently "
    << (src.getIsSigned() == true ? "signed" : "not signed") << std::endl
    << "Grade needed to sign: " << src.getReqGradeToSign() <<  std::endl
    << "Grade needed to execute: " << src.getReqGradeToExec();
    return out;
}

Form::HighGradeException::HighGradeException(void)
{
    return ;
}

Form::HighGradeException::HighGradeException(const HighGradeException &src)
{
    *this = src;
    return ;
}

Form::HighGradeException::~HighGradeException(void) throw()
{
    return ;
}

Form::HighGradeException &Form::HighGradeException::operator= (const HighGradeException &rhs)
{
    static_cast <void> (rhs);
    return (*this);
}

const char *Form::HighGradeException::what(void) const throw()
{
    return ("Grade is too high...");
}


Form::LowGradeException::LowGradeException(void)
{
    return ;
}

Form::LowGradeException::LowGradeException(const LowGradeException &src)
{
    *this = src;
    return ;
}

Form::LowGradeException::~LowGradeException(void) throw()
{
    return ;
}

Form::LowGradeException
&Form::LowGradeException::operator= (const LowGradeException &rhs)
{
    static_cast <void> (rhs);
    return (*this);
}

const char *Form::LowGradeException::what(void) const throw()
{
    return ("Grade is too low...");
}

Form::NotSignedException::NotSignedException(void)
{
    return ;
}

Form::NotSignedException::NotSignedException(const NotSignedException& src)
{
    *this = src;
    return ;
}

Form::NotSignedException::~NotSignedException(void) throw()
{
    return ;
}

Form::NotSignedException &Form::NotSignedException::operator= (const NotSignedException &rhs)
{
    static_cast <void> (rhs);
    return (*this);
}

const char*Form::NotSignedException::what(void) const throw()
{
    return ("Form not signed, yet");
}

