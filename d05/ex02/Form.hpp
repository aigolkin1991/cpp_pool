//
// Created by Artem Iholkin on 10/8/18.
//

#ifndef D05_FORM_HPP
#define D05_FORM_HPP

#include <iostream>
#include <string>
#include <stdexcept>
#include <fstream>
#include "Bureaucrat.hpp"

class Bureaucrat;

class Form{
    class HighGradeException : public std::exception
    {
    public:
        HighGradeException(void);
        HighGradeException(const HighGradeException &src);

        virtual ~HighGradeException(void) throw();

        HighGradeException &operator= (const HighGradeException &rhs);

        virtual const char    *what(void) const throw();
    };

    class LowGradeException : public std::exception
    {
    public:
        LowGradeException(void);
        LowGradeException(const LowGradeException &src);

        virtual ~LowGradeException(void) throw();

        LowGradeException  &operator= (const LowGradeException &rhs);

        virtual const char    *what(void) const throw();
    };

    class NotSignedException : public std::exception
    {
    public:
        NotSignedException(void);
        NotSignedException(const NotSignedException &src);

        virtual ~NotSignedException(void) throw();

        NotSignedException    &operator= (const NotSignedException &rhs);

        virtual const char    *what(void) const throw();
    };

public:
    Form(std::string name, unsigned int req_grade_to_sign, unsigned int req_grade_to_exec);
    Form(const Form &src);
    virtual ~Form(void);

    std::string     getName(void) const ;
    unsigned int    getReqGradeToSign(void) const ;
    unsigned int    getReqGradeToExec(void) const ;
    bool            getIsSigned(void) const ;
    void            beSigned(const Bureaucrat *b);
    void            execute(Bureaucrat const & executor) const;

    virtual void        action(std::string target) const = 0;
    virtual std::string getTarget(void) const = 0;

    Form&           operator=(const Form &src);

private:
    Form(void);

    const std::string       _name;
    bool                    _is_signed;
    const unsigned int      _req_grade_to_sign;
    const unsigned int      _req_grade_to_exec;
};

std::ostream&       operator<<( std::ostream &out, Form const &src );

#endif //D05_FORM_HPP
